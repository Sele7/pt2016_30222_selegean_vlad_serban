package Model;

import java.util.List;

public interface DictionaryInterface {
	
	/*
	 * @pre : (word != null) && (word.getName() != " ") sa nu fie map nu null si cuvantul sa nu fie spatiu text1 si text2 formeaza Word
	 * @post : words.size()++ se verifica ca sa adaugat **/
	public void addWord(String text1 , String text2);
	
	/*
	 * @pre : (word != null) && (word.getName() != " ") sa nu fie map nu null si cuvantul sa nu fie spatiu text1 si text2 formeaza Word
	 * @post : words.size() != 0 sa nu fie map u null **/
	public void updateWord(String text1 , String text2);
	
	/*
	 * @pre : (word != null) && (word.getName() != " ") sa nu fie map nu null si cuvantul sa nu fie spatiu text1 formeaza Word
	 * @post : words.size()-- se verifica ca sa sters **/
	public void deleteWord(String text1);
	/*
	 * @pre: words != null && text != null; sa nu fie map nu null si cuvantul sa nu fie null
	 * @post: foundWords != null lista in care punem cuvantu/cuvintele gasite sa fie !=null*/
	public List<Word> search(String text);
	
	/*
	 * @pre: words != null  sa nu fie map nu null 
	 * @post: sortedList != null; lista in care punem cuvintele gasite sa fie !=null*/
	public List<Word> getAllWords();
	
	/*
	 * @pre: words != null  sa nu fie map nu null 
	 * @post: assert isConsistent; in functie de isCOnsistent este sau nu OK*/
	public boolean checkConsistency();
	

}
