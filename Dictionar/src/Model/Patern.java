package Model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Patern {
	
	public static boolean regexCheck(String regex, String stringToCheck){
        regex = regex.replaceAll("\\?", ".");//rep a word
        regex = regex.replaceAll("\\*", ".*");//rep a string even null
       
        Pattern checkRegex = Pattern.compile(regex);
        Matcher regexMatcher = checkRegex.matcher(stringToCheck);
           
        return regexMatcher.matches();
    }

}
