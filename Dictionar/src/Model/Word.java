package Model;

import java.util.ArrayList;
import java.util.List;

public class Word {
	
	private String name;
	private String definition;
	

	
	public Word() {

	}
	
	public Word(String name, String definition) {
		super();
		this.name = name;
		this.definition = definition;
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
		
	}


	public String getDefinition() {
		return definition;
	}


	public void setDefinition(String definition) {
		this.definition = definition;
		
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	

}
