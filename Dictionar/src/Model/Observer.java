package Model;

import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Observer.
 */
public abstract  class Observer {
	
	/** The word. */
	
	protected Map<String, String> subject;
	
	/**
	 * Update.
	 */
	public abstract void update(int i);
}
