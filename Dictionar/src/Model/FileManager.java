package Model;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class FileManager {
	
	public static void saveToFile(Dictionary dictionary) throws JAXBException {
		File file = new File("Dictionar.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Dictionary.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(dictionary, file);
		jaxbMarshaller.marshal(dictionary, System.out);
	}

	public static Dictionary readFromFile(String fileName) throws JAXBException {
		File file = new File(fileName);
		JAXBContext jaxbContext = JAXBContext.newInstance(Dictionary.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Dictionary dictionary = (Dictionary) jaxbUnmarshaller.unmarshal(file);
		System.out.println(dictionary);
		return dictionary;
	}

}
