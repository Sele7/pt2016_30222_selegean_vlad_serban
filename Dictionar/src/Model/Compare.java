package Model;

import java.util.Comparator;

public class Compare implements Comparator<Word> {

	@Override
	public int compare(Word o1, Word o2) {
		// TODO Auto-generated method stub
		return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
	}

}
