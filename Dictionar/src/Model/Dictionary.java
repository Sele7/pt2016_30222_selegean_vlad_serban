package Model;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dictionary implements DictionaryInterface{
	
	@XmlElement
	private static Map<String,String> words = new HashMap<String,String>();
	
	

	/** The observers. */
	private List<Observer> observers = new ArrayList<Observer>();
	
	public Dictionary(){
		Obss obs = new Obss();
		obs.setSubject(this);
	}
	protected boolean isWellFormed()
	{
		if(words == null)
			return false;
		else
			return true;
	}

	public  Map<String,String> ret(){
		return words;
	}

	@Override
	public void addWord(String text1 , String text2) {
		// TODO Auto-generated method stub
		Word word = new Word(text1,text2);

		assert isWellFormed();
		assert ( (word != null) && (word.getName() != " "));
		
		int s1 = words.size();
		int s2=0;
		
		
		
		if(words.containsKey(word.getName()) != true)
		{
			
			words.put(word.getName(), word.getDefinition());
			s2 = words.size();
			
		}
		
		
		
		if(s2 == s1 + 1)
		{
			assert true;
			assert isWellFormed();
			notifyAllObservers(0);
			System.out.println("Add word Succeded");
		}
			
		else
		{
			System.out.println("Add word Failed");
			assert false;
		}
		
	}

	@Override
	public void updateWord(String text1 , String text2) {
		// TODO Auto-generated method stub
		Word word = new Word(text1,text2);

		assert isWellFormed();
		assert ( (word != null) && (word.getName() != " "));
		
		if(words.containsKey(word.getName()) == true)
		{
			
			words.put(word.getName(), word.getDefinition());
			
			assert(words.size() != 0);
			assert isWellFormed();
			notifyAllObservers(2);
			System.out.println("Update word Succeded");
		}
		else
		{
			System.out.println("Update word Failed");
			assert false;
		}
			
		
		
		
	}

	@Override
	public void deleteWord(String text1) {
		// TODO Auto-generated method stub
		Word word = new Word(text1,"nimic");
		assert isWellFormed();
		assert ( (word != null) && (word.getName() != " "));
		int s1 = words.size();
		if(words.containsKey(word.getName()) == true)
		{
			words.remove(word.getName());
		}
		
		int s2 = words.size();
		if(s2 == s1 - 1)
		{
			assert true;
			assert isWellFormed();
			notifyAllObservers(1);
			System.out.println("Remove word Succeded");
		}
			
		else
		{
			System.out.println("Remove word Failed");
			assert false;
		}
			
		
	
			
			
	}

	@Override
	public List<Word> search(String text) {
		// TODO Auto-generated method stub
		assert isWellFormed();
		assert words != null && text != null;
		
		List<Word> foundWords = new ArrayList<Word>();
		
		text.toLowerCase();
		
		
		if(!text.contains("*") && !text.contains("?"))
		{
			
			String found = words.get(text);
			Word matchWord = new Word(text,found);
			foundWords.add(matchWord);
		}
		else
		{
			Iterator<Map.Entry<String, String>> entries = words.entrySet().iterator();
			 while (entries.hasNext()) 
			 {
	                Map.Entry<String, String> entry = entries.next();
	                String currentWord = entry.getKey();
	                if (Patern.regexCheck(text, entry.getKey())) 
	                {
	                	Word matchWord = new Word(entry.getKey(), entry.getValue());
	                	foundWords.add(matchWord);
	                }
			 }
		}
		
		assert foundWords != null;
		assert isWellFormed();
		
		Collections.sort(foundWords, new Compare());
		if(foundWords.size() == 0)
		{
			System.out.println("Word Search Failed no such Word");
			assert false;
		}
			
		System.out.println("Word Search Succeded");
		return foundWords;
	}

	@Override
	public List<Word> getAllWords() {
		// TODO Auto-generated method stub
		assert isWellFormed();
		assert words != null;
		
	
		List<Word> sortedList = new ArrayList<Word>();
		
		for(Map.Entry<String, String> entry : words.entrySet())
		{
			Word word = new Word();
			word.setName(entry.getKey());
			word.setDefinition(entry.getValue());
			sortedList.add(word);
			
		}
		
		
		
		assert sortedList != null;
		assert isWellFormed();
		
		Collections.sort(sortedList,new Compare());
		
		if(sortedList.size() == 0)
		{
			System.out.println("All Word Print Failed no Words");
			assert false;
		}
			
		System.out.println("All Word Print Succeded");
		return sortedList;
	}

	@Override
	public boolean checkConsistency() {
		// TODO Auto-generated method stub
		
		assert isWellFormed();
		assert words != null;
		
		
		
		List<Word> definitii = new ArrayList<Word>();
		
		boolean isConsistent = false;
		
		for (Map.Entry<String, String> entry : words.entrySet()) {
			String definitie = entry.getValue();
			String[] defs = definitie.split(" ");
			
			for(int i=0 ; i < defs.length ;i++)
			{
				Word word = new Word();
				word.setName(defs[i]);
				word.setDefinition(defs[i]);
				System.out.println(defs[i]);
				definitii.add(word);
			}
		}
		
		for(Word word : definitii)
		{
			
			if((word.getName().equals("")) || (word.getName().equals(" ")))
				isConsistent = true;
			else
				for (Map.Entry<String, String> entry : words.entrySet())
					if(word.getName().equals(entry.getKey()))
					{
						System.out.println("Din def " + word.getName());
						System.out.println("Din map " + entry.getKey());
						isConsistent = true;
					}
					else
						isConsistent = false;
		}
		if (isConsistent == false)
		{
			System.out.println("Dictionary is not consistent!");
			definitii = new ArrayList<Word>();
			return false;
		}
		else
		{
			assert isConsistent;
			assert isWellFormed();
			definitii = new ArrayList<Word>();
			System.out.println("Dictionary is consistent!");
			return true;
		}
		
	}
	

	public static Map<String, String> getWords() {
		return words;
	}

	public static void setWords(Map<String, String> words) {
		Dictionary.words = words;
	}
	/**
 	 * Attach.
 	 *
 	 * @param observer the observer
 	 */
 	public void attach(Observer observer){
	      observers.add(observer);		
	   }

	   /**
   	 * Notify all observers.
   	 */
   	public void notifyAllObservers(int i){
	      for (Observer observer : observers) {
	    	  observer.update(i);
	      }
	   } 

}
