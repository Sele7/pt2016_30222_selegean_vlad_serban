package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class GUI {

	private JFrame frame;
	private JTextField textFieldNume;
	private JTextField textFieldDefinitie;
	private JTextField textFieldSearch;
	private JTextArea textArea;
	
	public JButton btnAddWord;
	public JButton btnRemoveW;
	public JButton btnUpdateW;
	public JButton btnSearch;
	public JButton btnCheckconst;
	public JButton btnGetAll;
	public JButton btnSave;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("DEX.RO");
		frame.setBounds(100, 100, 775, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textFieldNume = new JTextField();
		textFieldNume.setBounds(10, 31, 148, 20);
		frame.getContentPane().add(textFieldNume);
		textFieldNume.setColumns(10);
		
		textFieldDefinitie = new JTextField();
		textFieldDefinitie.setColumns(10);
		textFieldDefinitie.setBounds(186, 31, 148, 20);
		frame.getContentPane().add(textFieldDefinitie);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(10, 11, 46, 14);
		frame.getContentPane().add(lblNume);
		
		JLabel lblDefinitie = new JLabel("Definitie");
		lblDefinitie.setBounds(186, 11, 46, 14);
		frame.getContentPane().add(lblDefinitie);
		
		btnAddWord = new JButton("AddWord");
		btnAddWord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddWord.setBounds(364, 30, 125, 23);
		frame.getContentPane().add(btnAddWord);
		
		btnRemoveW = new JButton("RemoveW");
		btnRemoveW.setBounds(10, 62, 125, 23);
		frame.getContentPane().add(btnRemoveW);
		
		btnUpdateW = new JButton("UpdateW");
		btnUpdateW.setBounds(504, 30, 125, 23);
		frame.getContentPane().add(btnUpdateW);
		
		textFieldSearch = new JTextField();
		textFieldSearch.setColumns(10);
		textFieldSearch.setBounds(10, 151, 148, 20);
		frame.getContentPane().add(textFieldSearch);
		
		JLabel labelNumeSearch = new JLabel("Nume");
		labelNumeSearch.setBounds(10, 126, 46, 14);
		frame.getContentPane().add(labelNumeSearch);
		
		btnSearch = new JButton("Search");
		btnSearch.setBounds(20, 182, 89, 23);
		frame.getContentPane().add(btnSearch);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(186, 149, 563, 285);
		frame.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		btnCheckconst = new JButton("CheckConst");
		btnCheckconst.setBounds(20, 314, 89, 23);
		frame.getContentPane().add(btnCheckconst);
		
		btnGetAll = new JButton("GetAll");
		btnGetAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnGetAll.setBounds(20, 353, 89, 23);
		frame.getContentPane().add(btnGetAll);
		
		btnSave = new JButton("Save");
		btnSave.setBounds(20, 396, 89, 23);
		frame.getContentPane().add(btnSave);
	}
	
	public JTextField getFieldNume(){
		return textFieldNume;
	}
	
	public JTextField getFieldDefinitie(){
		return textFieldDefinitie;
	}
	
	public JTextArea getTextArea(){
		return textArea;
	}
	
	public JTextField getFieldSearch(){
		return textFieldSearch;
	}
	
	public JFrame getFrame(){
		return frame;
	}
	
	public void addWord(ActionListener listenForAdd){
		btnAddWord.addActionListener(listenForAdd);
	}
	
	public void deleteWord(ActionListener listenForDelete){
		btnRemoveW.addActionListener(listenForDelete);
	}
	
	public void updateWord(ActionListener listenForUpp){
		btnUpdateW.addActionListener(listenForUpp);
	}
	
	public void searchWord(ActionListener listenForSearch){
		btnSearch.addActionListener(listenForSearch);
	}
	
	public void checkConst(ActionListener listenForCheck){
		btnCheckconst.addActionListener(listenForCheck);
	}
	
	public void getAll(ActionListener listenForAll){
		btnGetAll.addActionListener(listenForAll);
	}
	
	public void saveDict(ActionListener listenForSave){
		btnSave.addActionListener(listenForSave);
	}
	
}
