package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.xml.bind.JAXBException;

import Model.Dictionary;
import Model.FileManager;
import Model.Word;
import View.GUI;

public class Controller {
	
	private GUI view;
	private Dictionary model;
	
	public Controller(GUI view , Dictionary model){
		this.view = view;
		this.model = model;
		
		this.view.addWord(new addAcc());
		this.view.deleteWord(new removeWord());
		this.view.updateWord(new updateWord());
		this.view.searchWord(new searchWord());
		this.view.checkConst(new checkCons());
		this.view.getAll(new printAll());
		this.view.saveDict(new save());
		
	}
	
	private class addAcc implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String text1 = view.getFieldNume().getText();
			String text2 = view.getFieldDefinitie().getText();
			model.addWord(text1, text2);
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
		}
		
	}
	
	private class removeWord implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			String text1 = view.getFieldNume().getText();
			model.deleteWord(text1);
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
		}
		
	}
	
	private class updateWord implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String text1 = view.getFieldNume().getText();
			String text2 = view.getFieldDefinitie().getText();
			model.updateWord(text1, text2);
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
		}
		
	}
	
	private class searchWord implements ActionListener{
		String s = "";
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String text1 = view.getFieldSearch().getText();
			List<Word> list = model.search(text1);
			int size = list.size();
			System.out.println("SIZE ESTE " + size);
			
			for(int i = 0 ; i < size ; i++)
			{
				System.out.println("NUME " + list.get(i).getName());
				System.out.println("DEF " + list.get(i).getDefinition());
				
				s = s + list.get(i).getName() + " = " + list.get(i).getDefinition() + "\n";
				//view.getTextArea().setText(model.search(text1).get(i).getName() +" "+ model.search(text1).get(i).getDefinition());
			}
			view.getTextArea().setText(s);
			s="";
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
		}
		
	}
	
	private class checkCons implements ActionListener{
		String txt1;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(model.checkConsistency() == true)
				 txt1 = "Dictionary is consistent!";
			else
				 txt1= "Dictionary is not consistent!";
			
			JOptionPane.showMessageDialog(view.getFrame(), txt1);
			//JOptionPane.showMessageDialog(view.getFrame(), model.checkConsistency());
		}
		
	}
	
	private class printAll implements ActionListener{
		String s = "";
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			clear();
			for(int i=0 ; i < model.getAllWords().size() ; i++)
			{
				
				s = s + model.getAllWords().get(i).getName() + " = " + model.getAllWords().get(i).getDefinition() + "\n" ;
				//view.getTextArea().setText(model.getAllWords().get(i).getName() + " " + model.getAllWords().get(i).getDefinition() + "\n" );
				
			}
			
			view.getTextArea().setText(s);
			s="";
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
			
		}
		
		
		
	}
	
	private class save implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			FileManager fm = new FileManager();
			
			try {
				FileManager.saveToFile(model);
			} catch (JAXBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(view.getFrame(), "Success ! ");
		}
		
	}
	
	public void clear(){
		view.getTextArea().setText(null);
	}

}
