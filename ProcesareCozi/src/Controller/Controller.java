package Controller;

import java.util.ArrayList;
import java.util.Random;


import Model.Client;
import Model.Coada;
import View.GUI;


// TODO: Auto-generated Javadoc
/**
 * The Class Controller. --- Clasa care se refera la " casele de marcat "
 */
public class Controller extends Thread {
	
	/** timpul de  servire minim. */
	private int timpServireMinim;
	
	/** timpul de servire maxim. */
	private int timpServireMaxim;
	
	/** timpul de sosire minim. */
	private int timpSosireMinim;
	
	/** timpul de sosire maxim. */
	private int timpSosireMaxim;
	
	/** timpul de servire mediu. */
	private double timpDeServireMediu=0;
	//private int timpDeAsteptareMediu=0;
	
	/** Numarul de cozi. */
	private int nrCozi;
	
	/** numarul de  clienti. */
	private int nrClienti;
	
	/** timpul  de servire. */
	private static int timpDeServire=0;
	
	/** timpul de asteptare. */
	private static int timpAsteptare = 0;
	
	/** timpul Real din Simulare */
	private static int timpReal = 0 ;
	
	/**  ora de varf. */
	private int oraDeVarf;
	
	/** numarul de clienti maxim. */
	private int numarClientiMaxim=0;
	
	/** o lista de clienti aici sunt adaugati clientii creati si cand ajung la casa sunt adaugati in coada */
	private ArrayList<Client> clienti = new ArrayList<>();
	
	/** Cozile */
	public Coada[] cozi;
	
	/** Random. */
	public Random random = new Random();
	
	/** timpul de simulare. */
	private int timpSimulare;
	
	/** timpul in care o coada e goala. */
	private int timpCoadaGoala=0;
	
	/** Text-ul care va fi afisat in textArea-ul din GUI */
	public static String text = "" ;
	
	/**
	 * Instantiates a new controller.
	 *
	 * @param timpServireMinim the timp servire minim
	 * @param timpServireMaxim the timp servire maxim
	 * @param timpSosireMinim the timp sosire minim
	 * @param timpSosireMaxim the timp sosire maxim
	 * @param nrCozi the nr cozi
	 * @param nrClienti the nr clienti
	 * @param timpSimulare the timp simulare
	 */
	/*
	 * public int randomInteger(int min, int max) {

    Random rand = new Random();

    // nextInt excludes the top value so we have to add 1 to include the top value
    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
}*/
	public Controller(int timpServireMinim, int timpServireMaxim, int timpSosireMinim, int timpSosireMaxim, int nrCozi,
			int nrClienti,int timpSimulare) {
		super();
		this.timpServireMinim = timpServireMinim;
		this.timpServireMaxim = timpServireMaxim;
		this.timpSosireMinim = timpSosireMinim;
		this.timpSosireMaxim = timpSosireMaxim;
		this.nrCozi = nrCozi;
		this.nrClienti = nrClienti;
		this.timpSimulare=timpSimulare;

		this.cozi = new Coada[nrCozi];
		
		for(int i = 0 ; i < nrCozi ; i++)
			cozi[i] = new Coada();

	}
	
	/**
	 * Returneza timpul simulare.
	 *
	 * @return the timp simulare
	 */
	public int getTimpSimulare()
	{
		return timpSimulare;
	}
	
	/**
	 * Seteaza timpul de  asteptare.
	 *
	 * @param timp the new timp asteptare
	 */
	public static void setTimpAsteptare(int timp)
	{
		timpAsteptare = timp;
	}
	
	/**
	 * Creste timp asteptare.
	 *
	 * @param timp the timp
	 */
	public static void cresteTimpAsteptare(int timp)
	{
		timpAsteptare = timpAsteptare + timp;
	}
	
	/**
	 * Scade timp asteptare.
	 *
	 * @param timp the timp
	 */
	public static void scadeTimpAsteptare(int timp)
	{
		timpAsteptare = timpAsteptare - timp;
	}
	
//	public static void cresteTimpDeServire(int timp)
//	{
//		timpDeServire = timpDeServire + timp;
//	}
/**
 * Returneaza text-ul
 *
 * @return the log
 */
//	
	public static String getLog()
	{
		return text;
	}
	
	/**
	 * Afiseaza logger.
	 *
	 * @param s the s
	 */
	public static void afiseazaLogger(String s)
	{
		text = s;
		GUI.getLogger().setText(text);
	}
	
	/**
	 * Remake logger.
	 *
	 * @param s the s
	 */
	public static void remakeLogger(String s)
	{
		text = text + s;
	}
	
	/**
	 * Gets the timp de servire.
	 *
	 * @return the timp de servire
	 */
	public int getTimpDeServire()
	{
		return timpDeServire;
	}
	
	/**
	 * Gets the timp asteptare.
	 *
	 * @return the timp asteptare
	 */
	public static int getTimpAsteptare()
	{
		return timpAsteptare;
	}
	
	/**
	 * Gets the timp real.
	 *
	 * @return the timp real
	 */
	public static int getTimpReal()
	{
		return timpReal;
	}
	
	
	/**
	 * Sets the cozi zero.
	 *
	 * @param que the new cozi zero
	 */
	//------ Metode folosite pentru JUnit
	public  void setCoziZero(Coada que)
	{
		cozi[0] = que;
	}
	
	/**
	 * Sets the cozi unu.
	 *
	 * @param que the new cozi unu
	 */
	public void setCoziUnu(Coada que)
	{
			cozi[1] = que;
	}
	
	/**
	 * Gets the coada zero.
	 *
	 * @return the coada zero
	 */
	public Coada getCoadaZero()
	{
		return cozi[0];
	}
	
	/**
	 * Gets the nr clienti.
	 *
	 * @return the nr clienti
	 */
	public int getNrClienti()
	{
		return nrClienti;
	}
	
	/**
	 * Gets the cozi.
	 *
	 * @return the cozi
	 */
	public Coada[] getCozi()
	{
		return cozi;
	}
	
	/**
	 * Gets the clienti.
	 *
	 * @return the clienti
	 */
	public ArrayList<Client> getClienti()
	{
		return clienti;
	}
	
	//----- Metode folosite pentru JUnit
	
	/**
	 * Afla coada minima , in care v-om pune clientul care vine
	 * Se verifica care coada are size-ul mai mic si daca sunt egale se verifica timpul de servire intial si se
	 * alege cea cu timpul de servire initial cel mai mic 
	 * @return the coada minima
	 */
	public int getCoadaMinima()
	{
		int numarCoada=0;
		int time1=0;
		int time2 = 0;
		
		int minim = cozi[0].getSize();
		ArrayList<Client> minim2 = cozi[0].getCoada() ;
		
		for(int i=1 ; i<cozi.length ; i++)
		{
			if(cozi[i].getSize() < minim)
				{	
					minim = cozi[i].getSize();
					minim2 = cozi[i].getCoada();
					numarCoada = i;
				}
			if(cozi[i].getSize() == minim)
			{
				for(int j = 0 ; j < cozi[i].getCoada().size() ; j++)
					time1 = time1 + cozi[i].getCoada().get(j).getTimpDeServire();
				for(int z = 0 ; z < minim ; z++)
					time2 = time2 + minim2.get(z).getTimpDeServire();
				
				if(time1 < time2)
					numarCoada = i;
			}
		}
		
		return numarCoada;
	}
	
	/**
	 * Se cauta ora de varf maxima pentru fiecare secunda de aceea se numeste -remake-
	 * Se cauta maximul de clienti din acel timp , luand in considerare toti clientii din orice coada
	 */
	public void remakeOraDeVarf()
	{
		int numarClienti = 0;
		
		for(int i = 0 ; i < cozi.length ; i++)
			numarClienti = numarClienti + cozi[i].getSize();
		
		if(numarClienti > numarClientiMaxim)
			{
				numarClientiMaxim = numarClienti;
				oraDeVarf = timpReal;
			}
		
	}
	
	/**
	 * Creaza clienti cu valori random de sosire si de servire
	 * acesti clienti sunt asezati in lista din acest controller de unde v or fi luati si adaugati la coada 
	 * cand timpul lor de sosire este acc cu timpul real din simulare
	 */
	public void createClients()
	{
		int i;
		int timpSosire, timpServire;
		for(i=0;i<nrClienti;i++)
		{
			timpSosire = timpSosireMinim + random.nextInt(timpSosireMaxim-timpSosireMinim);
			timpServire = timpServireMinim + random.nextInt(timpServireMaxim-timpServireMinim);
			Client c=new Client(i,timpSosire,timpServire);
			clienti.add(c);
			//System.out.println(c.getTimpSosire() + " " + c.getTimpDeServire());
			
			
		}
		for(i=0;i<nrCozi;i++)
		{
			new Thread (cozi[i]).start();
		}
	}
	
	/**
	 * Verifica daca o coada este goala si creste contorul
	 */
	public void checkIfEmpty()
	{
		//int nr=0;
		for(int i = 0 ; i < nrCozi ; i++)
		{
			if(cozi[i].getCoada().size() == 0)
				timpCoadaGoala++;
		}
		
//		if(nr == nrCozi)
//			timpCoadaGoala++;
			
	}
	
	/**
	 * Aflta timpul de servire mediu si anume aduna timpul de servire al fiecarui client si il imparte la nr de clienti
	 *
	 * @return the timp servire mediu
	 */
	public double getTimpServireMediu()
	{
		double suma = 0;
		for (int i = 0 ; i < clienti.size() ; i++)
		{
			suma =(double) (suma + clienti.get(i).getTimpDeServire());

		}
		
		timpDeServireMediu = suma/nrClienti;
		return timpDeServireMediu;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		createClients();/**Se creeaza clientii*/
		for(timpReal = 1 ; timpReal <= timpSimulare ; timpReal++)/**Incepe simularea  aplicatiei*/
		{

			
			//System.out.println("\n->Timpul Real este: " + timpReal + "\n");
			//System.out.println("OK 1");
			
			GUI.getTimpReal().setText(Integer.toString(timpReal));/**Se afiseaza timpul real in fiecare sec*/
			remakeLogger("\n->Timpul Real este: " + timpReal + "\n"); /**Adauga la textA timpul real in fiecare sec*/
			for(Client client:clienti)
			{
				//System.out.println(clienti);
				//System.out.println("OK 2");
				if(client.getTimpSosire() == timpReal)/**Daca un client are timpul de sosire = cu cel real atunci
				il punem la coada , desigur la cea mai mica*/
				{
					//System.out.println("OK 3");
					int coadaMin = getCoadaMinima();
					cozi[coadaMin].addClient(client);
					
					remakeLogger("Clientul " + client.getID()/** Se adauga la textA fiecare sosire*/
					+ " a juns la ora " + client.getTimpSosire() 
					+ " in coada  " + (coadaMin +1 )+ " si are service time "
					+ client.getTimpDeServire() + " secunde" +  "\n");
				}
			}
			try {
				sleep(10);/**sleep -ul este necesat pt a putea itera din nou lista. Se verifica daca cozile sunt goale*/
				checkIfEmpty();
				//remakeOraDeVarf();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				sleep(1000);/**Se face sleep 1000 pana la urm iteratie a timpului real*/
				remakeOraDeVarf();/** Se cauta ora de varf */
				GUI.getLogger().setText(getLog());/** se afiseaza ce avem in text */
				printCozi();/** Se printeaza ce avem in cozi in evolutia coziilor*/
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	
		String print = text;/** Luam o variabila in care punem peste ce avem deja in text datele urmatoare*/
		print = print + "\n Timp mediu de asteptare: " + (double)timpAsteptare/nrClienti  + 
				"\n Timp mediu de servire " + getTimpServireMediu() +  "\n Ora de varf: "+ oraDeVarf
				+ " au fost " + numarClientiMaxim +" clienti" + "\n Timpul Mediu in care cozile au fost goale este : " 
				+ (double)timpCoadaGoala/nrCozi + "\n";
			
		GUI.getLogger().setText(print);/** Afisam acum tot ce sa intamplat */
		text = ""; /**Resetam text-ul - nu este obligatoriu
		/*
		System.out.println("\n Timp mediu de asteptare: " + (double)timpAsteptare/nrClienti  + 
		"\n Timp mediu de servire " + getTimpServireMediu() +  "\n Ora de varf: "+ oraDeVarf + " au fost " + numarClientiMaxim +" clienti");
		
		
		System.out.println(" Timpul Mediu in care cozile au fost goale este : " + (double)timpCoadaGoala/nrCozi + "\n");
		*/
	}
	
	/**
	 * Printeaza cozile , mai exact ce clienti sunt in ele in evolutia coziilor
	 */
	public void printCozi()
	{
		//Client client;
		int i = 0;
		String s = "";
		while(i < nrCozi)
		{
			//for(int j = 0 ; j< cozi[i].getSize();j++)
			s=s+"( C" + (i+1) + " ) ";
			for(Client clients : cozi[i].getCoada() )
			{
				//client = cozi[i].getCoada().get(j);
				//System.out.println("Coada " + (i+1) + ": Client ID " + clients.getID() + " Timp Sosire cu " + clients.getTimpSosire() + " Timp De Servire Initial  " + clients.getTimpDeServire());
				s=s+clients.getID() + "[ Sos:" + clients.getTimpSosire() + " Serv:" + clients.getTimpDeServire() +" ] ";
			}
			s=s+"\n";
			i++;
		}
		GUI.getEvolutie().setText(s);
	}
	
	

}
