package View;

import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import Controller.Controller;


import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class GUI.
 */
public class GUI {

	/** The frame. */
	private JFrame frame;
	
	/** The text field. */
	private JTextField textField;
	
	/** The text field_1. */
	private JTextField textField_1;
	
	/** The text field_2. */
	private JTextField textField_2;
	
	/** The text field_3. */
	private JTextField textField_3;
	
	/** The text field_4. */
	private JTextField textField_4;
	
	/** The text field_5. */
	private JTextField textField_5;
	
	/** The text field_6. */
	private JTextField textField_6;
	
	/** The text field_7. */
	private static JTextField textField_7;
	
	/** The text area. */
	private static JTextArea textArea;
	
	/** The btn start. */
	public JButton btnStart;
	
	/** The text area_1. */
	private static JTextArea textArea_1;
	
	/** The lbl evolutie cozi. */
	private JLabel lblEvolutieCozi;

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 791, 743);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(142, 21, 140, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblTimpMinSosire = new JLabel("Timp Min. Sosire");
		lblTimpMinSosire.setBounds(10, 24, 122, 14);
		frame.getContentPane().add(lblTimpMinSosire);
		
		JLabel lblTimpMaxSosire = new JLabel("Timp Max. Sosire");
		lblTimpMaxSosire.setBounds(10, 61, 122, 14);
		frame.getContentPane().add(lblTimpMaxSosire);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(142, 58, 140, 20);
		frame.getContentPane().add(textField_1);
		
		JLabel lblTimpMinServire = new JLabel("Timp Min. Servire");
		lblTimpMinServire.setBounds(410, 24, 147, 14);
		frame.getContentPane().add(lblTimpMinServire);
		
		JLabel lblTimpMaxServire = new JLabel("Timp Max. Servire");
		lblTimpMaxServire.setBounds(410, 61, 147, 14);
		frame.getContentPane().add(lblTimpMaxServire);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(585, 21, 140, 20);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(585, 58, 140, 20);
		frame.getContentPane().add(textField_3);
		
		JLabel lblNumarulDeCozi = new JLabel("Numarul de Cozi");
		lblNumarulDeCozi.setBounds(10, 123, 122, 14);
		frame.getContentPane().add(lblNumarulDeCozi);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(142, 120, 140, 20);
		frame.getContentPane().add(textField_4);
		
		JLabel lblNumarulDeClienti = new JLabel("Numarul de Clienti");
		lblNumarulDeClienti.setBounds(410, 123, 165, 14);
		frame.getContentPane().add(lblNumarulDeClienti);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(585, 120, 140, 20);
		frame.getContentPane().add(textField_5);
		
		JLabel lblTimpSimulare = new JLabel("Timp Simulare");
		lblTimpSimulare.setBounds(10, 185, 122, 14);
		frame.getContentPane().add(lblTimpSimulare);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(142, 182, 140, 20);
		frame.getContentPane().add(textField_6);
		
		JLabel lblTimpReal = new JLabel("Timp Real");
		lblTimpReal.setBounds(410, 185, 147, 14);
		frame.getContentPane().add(lblTimpReal);
		
		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(585, 182, 140, 20);
		frame.getContentPane().add(textField_7);
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart();
			}
		});
		btnStart.setBounds(293, 154, 89, 23);
		frame.getContentPane().add(btnStart);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 246, 715, 202);
		frame.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JLabel lblLogger = new JLabel("Logger");
		lblLogger.setBounds(25, 227, 57, 14);
		frame.getContentPane().add(lblLogger);
		
		textArea_1 = new JTextArea();
		textArea_1.setBounds(10, 507, 715, 186);
		frame.getContentPane().add(textArea_1);
		
		lblEvolutieCozi = new JLabel("Evolutie Cozi");
		lblEvolutieCozi.setBounds(27, 483, 122, 14);
		frame.getContentPane().add(lblEvolutieCozi);
	}
	
	/**
	 * returneaza textArea-ul in care este afisata rularea aplicatiei
	 *
	 * @return the logger
	 */
	public static  JTextArea getLogger()
	{
		return textArea;
	}
	
	/**
	 * Returneaza text fieldui pt a afisa timpul real in fiecare secunda
	 *
	 * @return the timp real
	 */
	public static JTextField getTimpReal()
	{
		return textField_7;
	}
	
	/**
	 * Returneaza text_area-ul in care va fi afisata evolutia clientiilor in cozi
	 *
	 * @return the evolutie
	 */
	public static  JTextArea getEvolutie()
	{
		return textArea_1;
	}
	
	/**
	 * La apasarea butonului de start se preia datele si se constr controllerul.
	 */
	private void btnStart()
	{

					int timpServMin = (int)Integer.parseInt(textField_2.getText());
					int timpServMax = (int)Integer.parseInt(textField_3.getText());
					int timpSosMin = (int)Integer.parseInt(textField.getText());
					int timpSosMax = (int)Integer.parseInt(textField_1.getText());
					int nrCozi  = (int)Integer.parseInt(textField_4.getText());
					int nrClienti = (int)Integer.parseInt(textField_5.getText());
					int timpSim= (int)Integer.parseInt(textField_6.getText());

					// creare Gestionare

					Controller ctrl = new Controller(timpServMin, timpServMax, timpSosMin, timpSosMax, nrCozi, nrClienti,timpSim);
					ctrl.start();
					ctrl.setTimpAsteptare(0);

	}
	
}
