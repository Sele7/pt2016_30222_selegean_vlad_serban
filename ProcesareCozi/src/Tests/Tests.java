package Tests;

import org.junit.Test;

import Controller.Controller;
import Model.Client;
import Model.Coada;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
// TODO: Auto-generated Javadoc

/**
 * The Class Tests.
 */
public class Tests {
	
	/**
	 * Verifica daca clientul este adaugat la Coada.
	 *
	 * @return true, if successful
	 */
	public boolean verificaSosireClient()
	{
		boolean OK = false;
		Client c1 = new Client(1,13,10);
		Coada queue = new Coada();
		
		queue.addClient(c1);
		
		if(queue.getCoada().size() == 1)
			OK = true;
		else
			OK = false;
		
		return OK;
	}
	
	/**
	 * Verifica daca coada minima este returnata OK
	 *
	 * @return true, if successful
	 */
	public boolean verificaCoadaMinima()
	{
		boolean OK = false;
		
		Client c1 = new Client(1,13,10);
		Client c2 = new Client(2,14,11);
		
		Coada que = new Coada();
		Coada que2 = new Coada();
		
		que.addClient(c1);
		
		que2.addClient(c1);
		que2.addClient(c2);
		
		Controller ctrl  = new Controller(1, 3, 1, 3, 2, 5, 10);
		ctrl.setCoziZero(que);
		ctrl.setCoziUnu(que2);
		int nr = ctrl.getCoadaMinima();
		
		if(nr == 0)
			OK = true;
		else
			OK = false;
		
		return OK;
	}
	
	/**
	 * Verifica daca coada ramane goala dupa ce pleaca singurul client
	 *
	 * @return true, if successful
	 */
	public boolean verificaPlecare()
	{
		boolean OK = false;
		Controller ctrl  = new Controller(1, 2, 1, 2, 1, 1, 3);
		ctrl.start();
		
		if(ctrl.getCoadaZero().getSize() == 0)
			OK = true;
		else
			OK = false;
		
		return OK;
	}
	
	/**
	 * Verifica daca numarul specificat de clienti este creat
	 *
	 * @return true, if successful
	 */
	public boolean verificaNumarClienti()
	{
		boolean OK = false;
		Controller ctrl  = new Controller(1, 2, 1, 2, 1, 5, 3);
		ctrl.createClients();
		
		if(ctrl.getClienti().size() == 5)
			OK = true;
		else
			OK = false;
		
		return OK;
	}
	
	/**
	 * Verifica daca numarul de cozi introduse este OK
	 *
	 * @return true, if successful
	 */
	public boolean verificaNumarCozi()
	{
		boolean OK = false;
		Controller ctrl  = new Controller(1, 2, 1, 2, 4, 5, 3);
		ctrl.start();
		
		if(ctrl.getCozi().length == 4)
			OK = true;
		else
			OK = false;
		
		return OK;
	}
	
	
	

	/**
	 * Test.
	 */
	@Test
	public void test(){
//	assertTrue(verificaSosireClient());
//	assertTrue(verificaCoadaMinima());
//	assertTrue(verificaPlecare());
//	assertTrue(verificaNumarClienti());
//	assertTrue(verificaNumarCozi());
	}

}
