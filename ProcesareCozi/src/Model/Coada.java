package Model;

import java.util.ArrayList;

import Controller.Controller;

// TODO: Auto-generated Javadoc
/**
 * The Class Coada.
 */
public class Coada extends Thread {
	
	/** Coada . Coada de clienti */
	private ArrayList <Client> coada = new ArrayList <Client>();//lista de clienti
	
	/**
	 * Adauga un client la coada -ca si un push pe stack-
	 *
	 * @param client the client
	 */
	public void  addClient(Client client){
		coada.add(client);
	}
	
	/**
	 * Returneaza dimensiunea cozii
	 *
	 * @return the size
	 */
	public int getSize(){
		return coada.size();
	}
	
	/**
	 * Returneaza coada
	 *
	 * @return the coada
	 */
	public ArrayList <Client> getCoada(){
		return this.coada;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run(){
		while(true)
		{	
			if(coada.isEmpty()) /** Daca coada e goala sleep pt 1000s*/
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else
			{
				try {/** Daca coada nu e goala sleep pt 1000 * timp de servire si calculeaza la fiecare secunda */
					int i,j,z=coada.get(0).getTimpDeServire(),y=0;/** dimensiunea cozii si adauga daca e nevoie timp de asteptare*/
					for (i=coada.get(0).getTimpDeServire();i>0;i--)
					{
					//sleep(100 * coada.get(0).getTimpDeServire());
						
					sleep(1000);

					//for(int i=0;i<5;i++){
					
//					System.out.println("Clientul " +(coada.get(0).getID()) + " a plecat la " +
//							Controller.getTimpReal() +"\n");
				//}
					
					if(coada.size() > 1 && (y != z))/**conditia in plus apare deoarece cand clientul pleaca
					el nu este scos din coada numai dupa acest for , astfel size-ul cozii ramane acc si rezultatul ar fi cu 1 mai mare*/
					{
						
						
						for(j = coada.size() ; j >1 ; j--)
								Controller.cresteTimpAsteptare(1);
						y++;
									
					}
					

					
					/*
					if(coada.size()>1)
						{
							//Controller.cresteTimpAsteptare(coada.get(0).getTimpDeServire());
							Controller.cresteTimpAsteptare(1);
						}*/
					
					
					//System.out.println("Timp de asteptare " + Controller.getTimpAsteptare());
					}
					
//					System.out.println("Clientul " +(coada.get(0).getID()) + " a plecat la " +
//							Controller.getTimpReal() +"\n");
					
					Controller.remakeLogger("Clientul " +(coada.get(0).getID()) + " a plecat la " +
							Controller.getTimpReal() +"\n");
					
					
					coada.remove(0);/**Clientul de la "casa" pleaca */
					
				
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
