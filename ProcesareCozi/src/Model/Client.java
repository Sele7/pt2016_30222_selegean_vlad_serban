package Model;

// TODO: Auto-generated Javadoc
/**
 * Clasa Client -> 
 */
public class Client {
	
	/** ID-ul fiecarui client pentru ai putea deosebi */
	private int ID;
	
	/** timpul la care soseste clientul */
	private int timpSosire;
	
	/** timpul necesar de servire */
	private int timpDeServire;
	
	
	/**
	 * Constructorul
	 *
	 * @param iD the i d
	 * @param timpSosire the timp sosire
	 * @param timpDeServire the timp de servire
	 */
	public Client(int iD, int timpSosire, int timpDeServire) {
		super();
		ID = iD;
		this.timpSosire = timpSosire;
		this.timpDeServire = timpDeServire;
	}
	
	/**
	 * Returneaza id-ul.
	 *
	 * @return the id
	 */
	public int getID() {
		return ID;
	}
	
	/**
	 * Seateaza id-ul.
	 *
	 * @param iD the new ID
	 */
	public void setID(int iD) {
		ID = iD;
	}
	
	/**
	 * Returneaza timpul de sosire.
	 *
	 * @return the timpul de sosire
	 */
	public int getTimpSosire() {
		return timpSosire;
	}
	
	/**
	 * Seteaza timpul de sosire.
	 *
	 * @param timpSosire the new timp sosire
	 */
	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}
	
	/**
	 * Returneaza timpul de servire.
	 *
	 * @return the timp de servire
	 */
	public int getTimpDeServire() {
		return timpDeServire;
	}
	
	/**
	 * Seteaza timpul de servire.
	 *
	 * @param timpDeServire the new timp de servire
	 */
	public void setTimpDeServire(int timpDeServire) {
		this.timpDeServire = timpDeServire;
	}

	
}
