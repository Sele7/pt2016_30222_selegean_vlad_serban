package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import Model.Account;
import Model.Bank;
import Model.Person;
import View.GUI;


// TODO: Auto-generated Javadoc
/**
 * The Class Controller.
 */
public class Controller implements Serializable {
	
	/** The view. */
	private GUI view;
	
	/** The model. */
	private Bank model;
	
	/**
	 * Instantiates a new controller.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public Controller(GUI view , Bank model)
	{
		this.view = view;
		this.model = model;
		
		this.view.addAcc(new addAcc());
		this.view.deleteAcc(new deleteAcc());
		this.view.deposit(new deposit());
		this.view.withdraw(new withdraw());
		this.view.empower(new emp());
		this.view.unEmpower(new unEmp());
		this.view.transfer(new transfer());
		this.view.interogareSold(new interogareSold());
		this.view.check(new check());
		this.view.serializare(new serializare());
		
		
		
		listAcc();
	}
	
	
	/**
	 * List acc.
	 */
	private void listAcc()
	{
		TableFill fillTable = new TableFill(model);
		fillTable.insertRows();
		view.setTable(fillTable.getTable());
	}
	
	/**
	 * The Class addAcc.
	 */
	private class addAcc implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int sum=0 , age = 0;
			
			
			
			//view.getFieldSum().getText();
		
			
			sum = Integer.parseInt(view.getFieldSum1().getText());
			age = Integer.parseInt(view.getFieldAge().getText());
			
			model.addAccount(view.getFieldNume().getText(), view.getFieldPreNume().getText(), age, sum, view.getFieldType().getText());
			listAcc();
		}
		
	}
	
	/**
	 * The Class deleteAcc.
	 */
	private class deleteAcc implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr = Integer.parseInt(view.getFieldAccNumDel().getText());
			model.removeAccount(nr);
			listAcc();
		}
	}
	
	/**
	 * The Class deposit.
	 */
	private class deposit implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr = Integer.parseInt(view.getFieldAccNumDep().getText());
			int sum = Integer.parseInt(view.getFieldSum1().getText());
			model.depozitaSuma(nr, sum);
			listAcc();
		}
	}
	
	/**
	 * The Class withdraw.
	 */
	private class withdraw implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr = Integer.parseInt(view.getFieldAccNumDep().getText());
			int sum = Integer.parseInt(view.getFieldSum1().getText());
			model.retrageSuma(nr, sum);
			listAcc();
		}
	}
	
	/**
	 * The Class emp.
	 */
	private class emp implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr = Integer.parseInt(view.getFieldAccNumEmp().getText());
			int age = Integer.parseInt(view.getFieldAge().getText());
			Person p = new Person(view.getFieldNume().getText(),view.getFieldPreNume().getText(), age);
			model.empowerPerson(nr, p);
			listAcc();
		}
	}
	
	/**
	 * The Class unEmp.
	 */
	private class unEmp implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr = Integer.parseInt(view.getFieldAccNumEmp().getText());
			int age = Integer.parseInt(view.getFieldAge().getText());
			Person p = new Person(view.getFieldNume().getText(),view.getFieldPreNume().getText(), age);
			model.unEmpowerPerson(nr, p);
			listAcc();
		}
	}
	
	/**
	 * The Class transfer.
	 */
	private class transfer implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int nr1 = Integer.parseInt(view.getFieldAccNum1().getText());
			int nr2 = Integer.parseInt(view.getFieldAccNum2().getText());
			int sum = Integer.parseInt(view.getFieldSum().getText());
			
			model.transferMoney(nr1, nr2, sum);
			listAcc();
		}
	}
	
	/**
	 * The Class interogareSold.
	 */
	private class interogareSold implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextArea afis = new JTextArea();

			int nr = Integer.parseInt(view.getFieldInter().getText());
			afis.setText(model.interogareSold(nr));
			JOptionPane.showMessageDialog(GUI.frame, afis);
			listAcc();
		}
	}
	
	/**
	 * The Class check.
	 */
	private class check implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextArea afis = new JTextArea();
			
			int nr = Integer.parseInt(view.getFieldAccNumEmp().getText());
			
			for(Account account: model.getAccounts())
				if(account.getNumber() == nr)
				{
					
					for(int i = 0 ; i < account.getEmpoweredOwners().size();i++)
						afis.setText(" " + account.getEmpoweredOwners().get(i).getSurName() +" "+ account.getEmpoweredOwners().get(i).getName() + " " +
								account.getEmpoweredOwners().get(i).getAge());
				}
			
			
			JOptionPane.showMessageDialog(GUI.frame, afis);
			listAcc();
		}
	}
	
	/**
	 * The Class serializare.
	 */
	private class serializare implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			model.storeBank();
			JOptionPane.showMessageDialog(null, "Data Serialized");
		}
	}

}
