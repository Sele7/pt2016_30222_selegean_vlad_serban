package Controller;

import java.io.Serializable;
import java.util.HashSet;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Model.Account;
import Model.Bank;

// TODO: Auto-generated Javadoc
/**
 * The Class TableFill.
 */
public class TableFill implements Serializable{
	
	/** The accounts. */
	private HashSet<Account> accounts= new HashSet<Account>();
	
	/** The table. */
	private JTable table = new JTable();
	
	/** The dtm. */
	DefaultTableModel dtm = new DefaultTableModel(0, 0);
	
	/** The table head. */
	private String tableHead[] = { "Number", "Sum", "Person" , "Type","Empower" };
	
	/** The model. */
	Bank model;

	
	/**
	 * Instantiates a new table fill.
	 *
	 * @param model the model
	 */
	public TableFill(Bank model) {

		this.model = model;

	}
	
/**
 * Insert rows.
 */
public void insertRows() {
		
		//table = view.getTable();
		dtm.setColumnIdentifiers(tableHead);
		table.setModel(dtm);
		dtm.setRowCount(0);
		for (Account account : model.getAccounts()) {
			dtm.addRow(new Object[] { account.getNumber(),account.getSum(),account.getPerson().getSurName() +" " + account.getPerson().getName() +" "+ account.getPerson().getAge(),account.getType(),account.getEmpoweredOwners().size()});
			//table.setModel(table);
		}
		
	}

/**
 * Gets the table.
 *
 * @return the table
 */
public JTable getTable() {
	return this.table;
}
}
