package View;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

// TODO: Auto-generated Javadoc
/**
 * The Class GUI.
 */
public class GUI implements Serializable{

	/** The frame. */
	public static JFrame frame;
	
	/** The nume. */
	private JTextField nume;
	
	/** The pre nume. */
	private JTextField preNume;
	
	/** The age. */
	private JTextField age;
	
	/** The acc type. */
	private JTextField accType;
	
	/** The Account num delete. */
	private JTextField AccountNumDelete;
	
	/** The Account num dep. */
	private JTextField AccountNumDep;
	
	/** The Sum1. */
	private JTextField Sum1;
	
	/** The Account num emp. */
	private JTextField AccountNumEmp;
	
	/** The Acc num1. */
	private JTextField AccNum1;
	
	/** The Acc num2. */
	private JTextField AccNum2;
	
	/** The Sum tr. */
	private JTextField SumTr;
	
	/** The Interogare. */
	private JTextField Interogare;
	
	/** The table. */
	private JTable table;
	
	/** The scroll pane. */
	private JScrollPane scrollPane;
	
	/** The table head. */
	private String tableHead[] = { "Number", "Sum", "Person" , "Type","Empower" };
	
	/** The btn add new acc. */
	public JButton btnAddNewAcc;
	
	/** The btn deleteacc. */
	public JButton btnDeleteacc;
	
	/** The btn deposit. */
	public JButton btnDeposit;
	
	/** The btn withdraw. */
	public JButton btnWithdraw ;
	
	/** The btn empower. */
	public JButton btnEmpower;
	
	/** The btn unempower. */
	public JButton btnUnempower;
	
	/** The btn transfer. */
	public JButton btnTransfer;
	
	/** The btn interogare. */
	public JButton btnInterogare;
	
	/** The btn serializare. */
	public JButton btnSerializare;
	
	/** The btn check. */
	public JButton btnCheck;
	
	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Banking System");
		frame.setBounds(100, 100, 1250, 601);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		nume = new JTextField();
		nume.setBounds(10, 37, 121, 20);
		frame.getContentPane().add(nume);
		nume.setColumns(10);
		
		preNume = new JTextField();
		preNume.setColumns(10);
		preNume.setBounds(10, 87, 121, 20);
		frame.getContentPane().add(preNume);
		
		age = new JTextField();
		age.setColumns(10);
		age.setBounds(10, 141, 121, 20);
		frame.getContentPane().add(age);
		
		accType = new JTextField();
		accType.setColumns(10);
		accType.setBounds(10, 188, 121, 20);
		frame.getContentPane().add(accType);
		
		JLabel labelNume = new JLabel("Nume");
		labelNume.setBounds(10, 24, 61, 14);
		frame.getContentPane().add(labelNume);
		
		JLabel labelPrenume = new JLabel("Prenume");
		labelPrenume.setBounds(10, 68, 77, 14);
		frame.getContentPane().add(labelPrenume);
		
		JLabel labelAge = new JLabel("Age");
		labelAge.setBounds(10, 118, 46, 14);
		frame.getContentPane().add(labelAge);
		
		JLabel labelType = new JLabel("Type");
		labelType.setBounds(10, 172, 46, 14);
		frame.getContentPane().add(labelType);
		
		btnAddNewAcc = new JButton("AddNewAcc");
		btnAddNewAcc.setBounds(186, 114, 112, 23);
		frame.getContentPane().add(btnAddNewAcc);
		
		btnDeleteacc = new JButton("DeleteAcc");
		btnDeleteacc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteacc.setBounds(346, 114, 112, 23);
		frame.getContentPane().add(btnDeleteacc);
		
		AccountNumDelete = new JTextField();
		AccountNumDelete.setBounds(488, 115, 121, 20);
		frame.getContentPane().add(AccountNumDelete);
		AccountNumDelete.setColumns(10);
		
		JLabel lblAccountnumber = new JLabel("AccountNumber");
		lblAccountnumber.setBounds(494, 90, 115, 14);
		frame.getContentPane().add(lblAccountnumber);
		
		AccountNumDep = new JTextField();
		AccountNumDep.setBounds(10, 257, 121, 20);
		frame.getContentPane().add(AccountNumDep);
		AccountNumDep.setColumns(10);
		
		Sum1 = new JTextField();
		Sum1.setColumns(10);
		Sum1.setBounds(10, 306, 121, 20);
		frame.getContentPane().add(Sum1);
		
		JLabel lblAccountNumber = new JLabel("Account Number");
		lblAccountNumber.setBounds(10, 232, 121, 14);
		frame.getContentPane().add(lblAccountNumber);
		
		JLabel lblSum = new JLabel("Sum");
		lblSum.setBounds(10, 288, 46, 14);
		frame.getContentPane().add(lblSum);
		
		btnDeposit = new JButton("Deposit");
		btnDeposit.setBounds(186, 284, 112, 23);
		frame.getContentPane().add(btnDeposit);
		
		btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnWithdraw.setBounds(346, 284, 112, 23);
		frame.getContentPane().add(btnWithdraw);
		
		AccountNumEmp = new JTextField();
		AccountNumEmp.setColumns(10);
		AccountNumEmp.setBounds(186, 188, 121, 20);
		frame.getContentPane().add(AccountNumEmp);
		
		JLabel lblAccountNumber_1 = new JLabel("Account Number");
		lblAccountNumber_1.setBounds(186, 172, 112, 14);
		frame.getContentPane().add(lblAccountNumber_1);
		
		btnEmpower = new JButton("Empower");
		btnEmpower.setBounds(346, 187, 112, 23);
		frame.getContentPane().add(btnEmpower);
		
		btnUnempower = new JButton("UnEmpower");
		btnUnempower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnUnempower.setBounds(488, 187, 112, 23);
		frame.getContentPane().add(btnUnempower);
		
		AccNum1 = new JTextField();
		AccNum1.setBounds(10, 385, 121, 20);
		frame.getContentPane().add(AccNum1);
		AccNum1.setColumns(10);
		
		AccNum2 = new JTextField();
		AccNum2.setColumns(10);
		AccNum2.setBounds(10, 430, 121, 20);
		frame.getContentPane().add(AccNum2);
		
		JLabel lblAccountNumber_2 = new JLabel("Account Number1");
		lblAccountNumber_2.setBounds(10, 360, 121, 14);
		frame.getContentPane().add(lblAccountNumber_2);
		
		JLabel lblAccountNumber_3 = new JLabel("Account Number2");
		lblAccountNumber_3.setBounds(10, 416, 121, 14);
		frame.getContentPane().add(lblAccountNumber_3);
		
		SumTr = new JTextField();
		SumTr.setColumns(10);
		SumTr.setBounds(10, 478, 121, 20);
		frame.getContentPane().add(SumTr);
		
		JLabel label = new JLabel("Sum");
		label.setBounds(10, 461, 46, 14);
		frame.getContentPane().add(label);
		
		btnTransfer = new JButton("Transfer");
		btnTransfer.setBounds(186, 429, 112, 23);
		frame.getContentPane().add(btnTransfer);
		
		Interogare = new JTextField();
		Interogare.setBounds(346, 430, 112, 20);
		frame.getContentPane().add(Interogare);
		Interogare.setColumns(10);
	
		btnInterogare = new JButton("Interogare");
		btnInterogare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInterogare.setBounds(494, 429, 106, 23);
		frame.getContentPane().add(btnInterogare);
		
		JLabel label_1 = new JLabel("Account Number");
		label_1.setBounds(346, 416, 121, 14);
		frame.getContentPane().add(label_1);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(648, 90, 560, 360);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnSerializare = new JButton("Serializare");
		btnSerializare.setBounds(186, 516, 112, 23);
		frame.getContentPane().add(btnSerializare);
		
		btnCheck = new JButton("Check");
		btnCheck.setBounds(186, 219, 112, 23);
		frame.getContentPane().add(btnCheck);
	}
	
	/**
	 * Gets the field nume.
	 *
	 * @return the field nume
	 */
	public JTextField getFieldNume() {
		return nume;
	}
	
	/**
	 * Gets the field pre nume.
	 *
	 * @return the field pre nume
	 */
	public JTextField getFieldPreNume() {
		return preNume;
	}
	
	/**
	 * Gets the field age.
	 *
	 * @return the field age
	 */
	public JTextField getFieldAge() {
		return age;
	}
	
	/**
	 * Gets the field type.
	 *
	 * @return the field type
	 */
	public JTextField getFieldType() {
		return accType;
	}
	
	/**
	 * Gets the field acc num del.
	 *
	 * @return the field acc num del
	 */
	public JTextField getFieldAccNumDel() {
		return AccountNumDelete;
	}
	
	/**
	 * Gets the field acc num dep.
	 *
	 * @return the field acc num dep
	 */
	public JTextField getFieldAccNumDep() {
		return AccountNumDep;
	}
	
	/**
	 * Gets the field sum1.
	 *
	 * @return the field sum1
	 */
	public JTextField getFieldSum1() {
		return Sum1;
	}
	
	/**
	 * Gets the field acc num emp.
	 *
	 * @return the field acc num emp
	 */
	public JTextField getFieldAccNumEmp() {
		return AccountNumEmp;
	}
	
	/**
	 * Gets the field acc num1.
	 *
	 * @return the field acc num1
	 */
	public JTextField getFieldAccNum1() {
		return AccNum1;
	}
	
	/**
	 * Gets the field acc num2.
	 *
	 * @return the field acc num2
	 */
	public JTextField getFieldAccNum2() {
		return AccNum2;
	}
	
	/**
	 * Gets the field sum.
	 *
	 * @return the field sum
	 */
	public JTextField getFieldSum() {
		return SumTr;
	}
	
	/**
	 * Gets the field inter.
	 *
	 * @return the field inter
	 */
	public JTextField getFieldInter() {
		return Interogare;
	}
	
	/**
	 * Sets the table.
	 *
	 * @param table the new table
	 */
	public void setTable(JTable table)
	{
		scrollPane.setViewportView(table);
	}
	
	/**
	 * Adds the acc.
	 *
	 * @param listenForAdd the listen for add
	 */
	public void addAcc(ActionListener listenForAdd){
		btnAddNewAcc.addActionListener(listenForAdd);
		}
	
	/**
	 * Delete acc.
	 *
	 * @param listenForDelete the listen for delete
	 */
	public void deleteAcc(ActionListener listenForDelete){
		btnDeleteacc.addActionListener(listenForDelete);
		}
	
	/**
	 * Deposit.
	 *
	 * @param listenForDeposit the listen for deposit
	 */
	public void deposit(ActionListener listenForDeposit){
		btnDeposit.addActionListener(listenForDeposit);
		}
	
	/**
	 * Withdraw.
	 *
	 * @param listenForWithdraw the listen for withdraw
	 */
	public void withdraw(ActionListener listenForWithdraw){
		btnWithdraw.addActionListener(listenForWithdraw);
		}
	
	/**
	 * Empower.
	 *
	 * @param listenForEmpower the listen for empower
	 */
	public void empower(ActionListener listenForEmpower){
		btnEmpower.addActionListener(listenForEmpower);
		}
	
	/**
	 * Un empower.
	 *
	 * @param listenForUnEmpower the listen for un empower
	 */
	public void unEmpower(ActionListener listenForUnEmpower){
		btnUnempower.addActionListener(listenForUnEmpower);
		}
	
	/**
	 * Transfer.
	 *
	 * @param listenForTransfer the listen for transfer
	 */
	public void transfer(ActionListener listenForTransfer){
		btnTransfer.addActionListener(listenForTransfer);
		}
	
	/**
	 * Interogare sold.
	 *
	 * @param listenForinterogareSold the listen forinterogare sold
	 */
	public void interogareSold(ActionListener listenForinterogareSold){
		btnInterogare.addActionListener(listenForinterogareSold);
		}
	
	/**
	 * Check.
	 *
	 * @param listenForCheck the listen for check
	 */
	public void check(ActionListener listenForCheck){
		btnCheck.addActionListener(listenForCheck);
		}
	
	/**
	 * Serializare.
	 *
	 * @param listenForserializare the listen forserializare
	 */
	public void serializare(ActionListener listenForserializare){
		btnSerializare.addActionListener(listenForserializare);
		}

}
