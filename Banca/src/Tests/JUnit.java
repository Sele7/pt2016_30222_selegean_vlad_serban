package Tests;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;

import Controller.Controller;
import Model.Account;
import Model.Bank;
import Model.Person;

// TODO: Auto-generated Javadoc
/**
 * The Class JUnit.
 */
public class JUnit {


		/**
		 * Adaugare account.
		 *
		 * @return true, if successful
		 */
		public boolean adaugareAccount()
		{
			boolean OK = false;
			Person p = new Person("sele","vlad",20);
			Account a = new Account(1,100,p);
			HashSet<Account> accounts= new HashSet<Account>();
			
			Bank b = new Bank();
			b.setAccounts(accounts);
			
			b.addAccount("sele", "vlad", 20, 300, "Spending");
			System.out.println(b.toString());
			if(accounts.size() == 1)
				OK = true;
			else
				OK = false;
			
			return OK;
		}
		
		
		/**
		 * Removes the account.
		 *
		 * @return true, if successful
		 */
		public boolean removeAccount()
		{
			boolean OK = false;
			
			Person p = new Person("sele","vlad",20);
			Account a = new Account(1,100,p);
			HashSet<Account> accounts= new HashSet<Account>();
			ArrayList<Account> currentAcc = new ArrayList<Account>();
			
			Bank b = new Bank();
			b.setAccounts(accounts);
			b.setCurrentAcc(currentAcc);
			
			b.addAccount("sele", "vlad", 20, 100, "Spending");
			System.out.println(b.toString());
			b.removeAccount(0);
			System.out.println(b.toString());
			if(accounts.size() == 0)
				OK = true;
			else
				OK = false;
			
			return OK;
		}
		
		/**
		 * Deposit.
		 *
		 * @return true, if successful
		 */
		public boolean deposit()
		{
			boolean OK = false;
			
			//Person p = new Person("sele","vlad",20);
			//Account a = new Account(1,100,p);
			HashSet<Account> accounts= new HashSet<Account>();
			ArrayList<Account> currentAcc = new ArrayList<Account>();
			
			Bank b = new Bank();
			b.setAccounts(accounts);
			
			b.addAccount("sele", "vlad", 20, 100, "Spending");
			System.out.println(b.toString());
			
			b.depozitaSuma(0, 100);
			
			for(Account account: accounts)
				if(account.getNumber() == 0)
					if(account.getSum() == 200)
						OK = true;
					else
						OK = false;
			
			
			return OK;
		}
		
		/**
		 * Withdraw.
		 *
		 * @return true, if successful
		 */
		public boolean withdraw()
		{
			boolean OK = false;
			
			//Person p = new Person("sele","vlad",20);
			//Account a = new Account(1,100,p);
			HashSet<Account> accounts= new HashSet<Account>();
			ArrayList<Account> currentAcc = new ArrayList<Account>();
			
			Bank b = new Bank();
			b.setAccounts(accounts);
			
			b.addAccount("sele", "vlad", 20, 200, "Spending");
			System.out.println(b.toString());
			
			b.retrageSuma(0, 100);
			
			for(Account account: accounts)
				if(account.getNumber() == 0)
					if(account.getSum() == 100)
						OK = true;
					else
						OK = false;
			
			
			return OK;
		}
		
		/**
		 * Transfer.
		 *
		 * @return true, if successful
		 */
		public boolean transfer()
		{
			boolean OK = false;
			
			//Person p = new Person("sele","vlad",20);
			//Account a = new Account(1,100,p);
			HashSet<Account> accounts= new HashSet<Account>();
			ArrayList<Account> currentAcc = new ArrayList<Account>();
			
			Bank b = new Bank();
			b.setAccounts(accounts);
			
			b.addAccount("sele", "vlad", 20, 200, "Spending");
			b.addAccount("seles", "vlads", 20, 300, "Spending");
			System.out.println(b.toString());
			
			b.transferMoney(0, 1, 100);
			
			for(Account account: accounts)
			{
				if(account.getNumber() == 0)
					if(account.getSum() == 100)
						OK = true;
					else
						OK = false;
				
				if(account.getNumber() == 1)
					if(account.getSum() == 400)
						OK = true;
					else
						OK = false;
			}
			
			
			return OK;
		}
		
		
		

		/**
		 * Test.
		 */
		@Test
		public void test(){
//		assertTrue(adaugareAccount());
//		assertTrue(removeAccount());
//		assertTrue(deposit());
//		assertTrue(withdraw());
//		assertTrue(transfer());
		}

	}



