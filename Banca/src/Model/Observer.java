package Model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Observer.
 */
public abstract class Observer implements Serializable {
	 
 	/** The subject. */
 	protected Account subject;
	 
 	/**
 	 * Update.
 	 */
 	public abstract void update();
}
