package Model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class SpendingAccount.
 */
public class SpendingAccount extends Account implements Serializable {

	/**
	 * Instantiates a new spending account.
	 *
	 * @param number the number
	 * @param sum the sum
	 * @param person the person
	 */
	public SpendingAccount(int number, int sum, Person person) {
		super(number, sum, person);
		this.type = "Spending";
		// TODO Auto-generated constructor stub
	}

}
