package Model;

import java.io.Serializable;
import java.util.Observer;

// TODO: Auto-generated Javadoc
/**
 * The Class ObserverPatternDemo.
 */
public class ObserverPatternDemo implements Serializable {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
	      Account subject = new Account(0, 0, null);
	      
	      Person p = new Person(null, null, 0);
	      p.setSubject(subject);
	      
	      subject.setSum(10);
	      subject.setSum(15);
	   }
}
