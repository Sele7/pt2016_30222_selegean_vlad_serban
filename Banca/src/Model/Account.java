package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class Account.
 */
public class Account implements Serializable{
	
	/** The number. */
	private int number;
	
	/** The sum. */
	private int sum;
	
	/** The person. */
	private Person person;
	
	/** The type. */
	protected String type;
	
	/** The empowered owners. */
	private ArrayList<Person> empoweredOwners = new ArrayList<Person>();
	
	/** The observers. */
	private List<Observer> observers = new ArrayList<Observer>();

	
	/**
	 * Instantiates a new account.
	 *
	 * @param number the number
	 * @param sum the sum
	 * @param person the person
	 */
	public Account(int number, int sum, Person person) {
		super();
		this.number = number;
		this.sum = sum;
		this.person = person;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Gets the sum.
	 *
	 * @return the sum
	 */
	public int getSum() {
		return sum;
	}

	/**
	 * Sets the sum.
	 *
	 * @param sum the new sum
	 */
	public void setSum(int sum) {
		this.sum = sum;
		notifyAllObservers();
	}

	/**
	 * Gets the person.
	 *
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * Sets the person.
	 *
	 * @param person the new person
	 */
	public void setPerson(Person person) {
		this.person = person;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
	/**
	 * Gets the empowered owners.
	 *
	 * @return the empowered owners
	 */
	public ArrayList<Person> getEmpoweredOwners() {
		return empoweredOwners;
	}

	/**
	 * Sets the empowered owners.
	 *
	 * @param empoweredOwners the new empowered owners
	 */
	public void setEmpoweredOwners(ArrayList<Person> empoweredOwners) {
		this.empoweredOwners = empoweredOwners;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "Account de tip  " + type + " AccountNumber " + number + " Sum " + sum +  " owner " + person.getSurName() + " " + person.getName() +" " + " age " + person.getAge();
	}

	 /**
 	 * Attach.
 	 *
 	 * @param observer the observer
 	 */
 	public void attach(Observer observer){
	      observers.add(observer);		
	   }

	   /**
   	 * Notify all observers.
   	 */
   	public void notifyAllObservers(){
	      for (Observer observer : observers) {
	    	  observer.update();
	      }
	   } 


}
