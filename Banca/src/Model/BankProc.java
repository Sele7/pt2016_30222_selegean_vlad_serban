package Model;

public interface BankProc{
	
	/**
	 * @pre : p != null && (type = "Saving" || type = "Spending") && accounts != null
	 * 	Sa existe un obiect  persoana iar type sa fie Spending sau Saving
	 * 
	 * @post : Adaugarea contului in Bank, accounts.size()++
	 * */
	public void addAccount(String nume,String prenume,int age, int sum, String type);
	
	/**
	 * @pre : accounts != null ** accountNumber != 0 
	 * 	Sa existe un obiect de tip Bank(Hashset) si a Contului + numarulContului sa fie dif de 0
	 * 
	 * @post : Stergerea contului. accounts.size()--
	 * */
	public void removeAccount(int accountNumber);
	
	/**
	 * @pre : nu sunt din punct de vedere al codului
	 * Sa existe un obiect Bank,Account si Person
	 * 
	 * @post: Introducerea datelor in HashSet*/
	public void generate();
	
	/**
	 * @pre : acounts != null;
	 * Sa existe o Banca-Hashset-ul
	 * 
	 * @post : Afisarea  @nochange */ 
	public String toString();
	
	/**
	 * @pre: accounts != null && accountNumber != 0 && sum > 0  
	 * 	sa existe Hashset-ul , numarulContului sa fie diferit de 0 iar suma adaugata sa fie mai mare ca 0
	 * 
	 * @post: se adauga suma in cont ... account.getSum() = account.getSum()-precedent- + sum*/
	public void depozitaSuma(int accountNumber ,int sum);
	
	/**
	 * @pre : accounts != null && accountNumber != 0 && sum > 0 
	 * 	sa existe Hashset-ul , numarulContului sa fie diferit de 0 iar suma adaugata sa fie mai mare ca 0
	 * 
	 * @post : se retrage suma din cont daca exista destul sold....account.getSum() = account.getSum()-precedent- - sum*/
	public void retrageSuma(int accountNumber, int sum);
	
	/**
	 *@pre: accounts != null && accountNumber !=0
	 *	Sa existe HashSet-ul si numarulContului sa fie diferit de 0 
	 *
	 *@post: @nochange - se afiseaza soldu current...*/
	public String interogareSold(int accountNumber);
	
	/**
	 *@pre : accounts != null && accountNumber !=0 && p!= null
	 *	Sa existe HashSet-ul si numarulContului sa fie diferit de 0 si persoana sa fie dif de null
	 *
	 *@post: persoana p nu mai este inputernicita adica .remove din lista empoweredOwners ...empoweredOwners.size()--*/
	public void unEmpowerPerson(int accountNumber , Person p);
	
	/**
	 *@pre : accounts != null && accountNumber !=0 && p!= null
	 *	Sa existe HashSet-ul si numarulContului sa fie diferit de 0 si persoana sa fie dif de null
	 *
	 *@post: persoana p este inputernicita adica .add in lista empoweredOwners....empoweredOwners.size()++ */
	public void empowerPerson(int accountNumber , Person p);
	
	/**
	 *@pre : sum >0 && accountNumber1 != 0 && accountNumber2 != 0 && accounts != null
	 * Sa exista HashSet-ul , numerele conturilor sa fie dif de 0 si suma de transferat sa fie mai mare de 0
	 * 
	 * @post : se transfera suma din contul sursa, contului destinatie ...acc1.getSum = acc1.getSum-prec- - sum ; si acc2.getSum = acc2.getSum-prec- + sum ; */
	public void transferMoney(int accountNumber1 , int accountNumber2 , int sum);

}
