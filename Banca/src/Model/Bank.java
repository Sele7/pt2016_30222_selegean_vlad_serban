package Model;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;;

// TODO: Auto-generated Javadoc
/**
 * The Class Bank.
 */
public class Bank implements BankProc,Serializable{
	
	/** The accounts. */
	HashSet<Account> accounts= new HashSet<Account>();
	
	/** The current acc. */
	ArrayList<Account> currentAcc = new ArrayList<Account>();
	
	/** The acc number. */
	int accNumber = 0;
	
	/**
	 * Checks if is well formed.
	 *
	 * @return true, if is well formed
	 */
	protected boolean isWellFormed()
	{
		if(accounts == null)
			return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see Model.BankProc#addAccount(java.lang.String, java.lang.String, int, int, java.lang.String)
	 */
	@Override
	public void addAccount(String nume,String prenume,int age, int sum, String type) {
		// TODO Auto-generated method stub
		Person p = new Person(nume,prenume,age);
		assert isWellFormed();
		assert (p != null && age >= 18 && (type.equals("Saving") || type.equals("Spending")));
		if(type.equals("Saving"))
		{
			SavingAccount a1 = new SavingAccount(accNumber,sum,p);
			p.setSubject(a1);
			accounts.add(a1);
			accNumber++;
		}
		else if (type.equals("Spending"))
		{
			SpendingAccount a1 = new SpendingAccount(accNumber,sum,p);
			p.setSubject(a1);
			accounts.add(a1);
			accNumber++;
		}
		assert (accounts.size() != 0);
		assert isWellFormed();
		System.out.println("addAccount Succeded !");
	}
	
	/* (non-Javadoc)
	 * @see Model.BankProc#removeAccount(int)
	 */
	@Override
	public void removeAccount(int accountNumber) {
		// TODO Auto-generated method stub
		//System.out.println(accounts);
		int s1 = accounts.size();
		assert isWellFormed();
		assert accounts != null && accountNumber != 0 ;
		int ok = 0;
		for(Account account : accounts)
		{
			if(account.getNumber() == accountNumber)
			{
				currentAcc.add(account);
				System.out.println("add to list Done !");
				ok = 1;
			}
			else
				if(ok == 0)
					System.out.println("add to list Failled !");
		}
		
		
		accounts.removeAll(currentAcc);
		int s2 = accounts.size();
		if(s2 == s1 - 1)
			assert true;
		else
			assert false;
		
		assert isWellFormed();
		System.out.println("Account Removed Succesful !");
		//System.out.println(accounts);
		
	}

	/* (non-Javadoc)
	 * @see Model.BankProc#generate()
	 */
	@Override
	public void generate() {
		// TODO Auto-generated method stub
		
		Person p1 = new Person("Sele","Vlad",20);
		Person p2 = new Person("Vali","Bogdan",20);
		
		SavingAccount a1 = new SavingAccount(1,100,p1);
		SpendingAccount a2 = new SpendingAccount(2,200,p2);
		
		p1.setSubject(a1);
		p2.setSubject(a2);
		accounts.add(a1);
		accounts.add(a2);
		
		accNumber = accounts.size()+1;
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		
		assert isWellFormed();
		assert accounts != null;
		
		String s = "";
		
		for(Account account : accounts)
		{
			s = s + account.toString();
			s = s + "\n";
		}
		
		assert isWellFormed();
		return s;
		
	}

	/* (non-Javadoc)
	 * @see Model.BankProc#depozitaSuma(int, int)
	 */
	@Override
	public void depozitaSuma(int accountNumber, int sum) {
		// TODO Auto-generated method stub
		int s1=0,s2=0;
		assert isWellFormed();
		assert (sum > 0 && accountNumber != 0 && accounts != null);
		int ok = 0;
		for(Account account : accounts)
		{
			if(account.getNumber() == accountNumber)
			{
				s1 = account.getSum();
				account.setSum(account.getSum() + sum);
				System.out.println("Funds added");
				s2 = account.getSum();
				ok = 1;
			}
				
		}
		
		if(ok == 0)
		{
			System.out.println("Adding Funds failed , Account does not exist");
			assert false;
		}
		
		
		if( s2 == s1 + sum )
			assert true;
		else
			assert false;
		assert isWellFormed();
		
		System.out.println("Operation was Succesful !");
	}
	
	/* (non-Javadoc)
	 * @see Model.BankProc#retrageSuma(int, int)
	 */
	@Override
	public void retrageSuma(int accountNumber, int sum){
		// TODO Auto-generated method stub
		int s1=0,s2=0;
		assert isWellFormed();
		assert (sum > 0 && accountNumber != 0 && accounts != null);

		int ok = 0;
		for(Account account : accounts)
		{
			if(account.getNumber() == accountNumber)
			{
				
				int sum1 = account.getSum();
				if((sum1 - sum) < 0 )
				{
					System.out.println("Not enough funds , funds left : " + sum1);
					assert false;
					
				}
				else if(account.getType().equals("Saving"))
				{
					System.out.println("This is a savings account , you can not withdraw money ! ");
					assert false;
					
				}
				else
				{
					s1 = account.getSum();
					account.setSum(account.getSum() - sum);
					System.out.println("Funds Withdrawed");
					ok = 1;
					s2 = account.getSum();
				}
					
			}
				
		}
		
		if(ok == 0)
		{
			System.out.println("Withdrawing  Funds failed, Account does not exist");
			assert false;
		}
			
		if( s2 == s1 - sum )
			assert true;
		else
			assert false;
		assert isWellFormed();
		System.out.println("Operation was Succesful !");
	}
	
	/* (non-Javadoc)
	 * @see Model.BankProc#interogareSold(int)
	 */
	@Override
	public String interogareSold(int accountNumber)
	{
		assert isWellFormed();
		assert (accountNumber != 0 && accounts != null);
		for(Account account: accounts)
			if(account.getNumber() == accountNumber)
			{
				String sum= Integer.toString(account.getSum());
				
				System.out.print("Operation Done ! ");
				return "Soldul este : " + sum;
			}
		assert isWellFormed();
		return "Account does not exist";
		
	}

	/* (non-Javadoc)
	 * @see Model.BankProc#unEmpowerPerson(int, Model.Person)
	 */
	@Override
	public void unEmpowerPerson(int accountNumber, Person p) {
		// TODO Auto-generated method stub
		int s1=0,s2=0;
		assert isWellFormed();
		assert ( p != null && accountNumber != 0 && accounts != null);
		
		int ok = 0;
		for(Account account: accounts)
			if(account.getNumber() == accountNumber)
			{
				
				for(int i = 0 ; i < account.getEmpoweredOwners().size();i++)
					if(account.getEmpoweredOwners().get(i).getName().equals(p.getName()))
					{
						s1 = account.getEmpoweredOwners().size();
						account.getEmpoweredOwners().remove(i);
						s2 = account.getEmpoweredOwners().size();
					}
						

				System.out.print("Operation Done ! ");
				
				ok = 1;
				
			}
		
		if(ok == 0)
		{
			System.out.println(" Account does not exist");
			assert false;
		}
		
		if(s2 == s1 - 1)
			assert true;
		else
			assert false;
		assert isWellFormed();
		System.out.println("Operation was Succesful !");
	}

	/* (non-Javadoc)
	 * @see Model.BankProc#empowerPerson(int, Model.Person)
	 */
	@Override
	public void empowerPerson(int accountNumber, Person p) {
		// TODO Auto-generated method stub
		int s1=0,s2=0;
		assert isWellFormed();
		assert ( p != null && accountNumber != 0 && accounts != null);
		List<Account> acc = new ArrayList<Account>(accounts);
		
		int j = 0;
		for(int i = 0 ; i < acc.size();i++)
		{
			if(acc.get(i).getNumber() == accountNumber)
			{
				s1 = acc.get(i).getEmpoweredOwners().size();
				acc.get(i).getEmpoweredOwners().add(p);
				s2 = acc.get(i).getEmpoweredOwners().size();
			}
				
		}
			

		System.out.println("Operation Done ! ");
		
		if(s2 == s1 + 1)
			assert true;
		else
			assert false;
		assert isWellFormed();
		System.out.println("Operation was Succesful !");
	}

	/* (non-Javadoc)
	 * @see Model.BankProc#transferMoney(int, int, int)
	 */
	@Override
	public void transferMoney(int accountNumber1, int accountNumber2, int sum) {
		// TODO Auto-generated method stub
		int s1=0,s2 = 0,s3 = 0,s4 = 0;
		assert isWellFormed();
		assert ( sum >0 && accountNumber1 != 0 && accountNumber2 != 0 && accounts != null);
		int ok = 0;
		for(Account account : accounts)
		{
			if(account.getNumber() == accountNumber1 && account.getSum() >= sum && account.getType().equals("Spending"))
			{
				s1 = account.getSum();
				for(Account account2 : accounts)
					if(account2.getNumber() == accountNumber2)
					{
						s3 = account2.getSum();
						account.setSum(account.getSum() - sum);
						s2 = account.getSum();
						account2.setSum(account2.getSum() + sum);
						System.out.println("Transfer Done ! ");
						ok = 1;
						s4 = account2.getSum();
					}
					
			}
		}
		
		if(ok == 0)
			System.out.println("Transfer Failed ! ");
		
		if(s2 == s1 -sum)
			if(s4 == s3 + sum)
				assert true;
			else
				assert false;
		assert isWellFormed();
		System.out.println("Operation was Succesful !");
	}
	
	/**
	 * Gets the accounts.
	 *
	 * @return the accounts
	 */
	public HashSet<Account> getAccounts() {
		return accounts;
	}
	
	/**
	 * Sets the accounts.
	 *
	 * @param accounts the new accounts
	 */
	public void setAccounts(HashSet<Account> accounts) {
		this.accounts = accounts;
	}

	/**
	 * Gets the current acc.
	 *
	 * @return the current acc
	 */
	public ArrayList<Account> getCurrentAcc() {
		return currentAcc;
	}
	
	/**
	 * Sets the current acc.
	 *
	 * @param currentAcc the new current acc
	 */
	public void setCurrentAcc(ArrayList<Account> currentAcc) {
		this.currentAcc = currentAcc;
	}
	
	/**
	 * Store bank.
	 */
	public void storeBank() {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("bank.ser"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            outputStream.writeObject(accounts);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
	/**
	 * Load warehouse. - Deserializarea TreeMapului
	 */
	public void loadBank() {
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("bank.ser");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream (fileIn);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Object obj;
        try {
            obj = inputStream.readObject();
            if (obj instanceof HashSet)
            {
                this.setAccounts((HashSet<Account>) obj);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	

	
	
	
	

}
