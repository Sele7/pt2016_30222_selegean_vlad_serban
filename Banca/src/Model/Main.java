package Model;

import java.util.ArrayList;
import java.util.List;

import Controller.Controller;
import View.GUI;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		Bank bank=new Bank();
		GUI view = new GUI();
		
		view.frame.setVisible(true);
		
		bank.generate();
		Person p1 = new Person("Viso","Adrian",20);
		Person p2 = new Person("Deac","Dan",20);
		
		bank.empowerPerson(1, p2);
		//bank.empowerPerson(2, p1);
		
		bank.loadBank();
		
		Controller c = new Controller(view,bank);
		System.out.println(bank.toString());
		//bank.removeAccount(1);
		//bank.addAccount("Viso","Adrian",20,100, "Saving");
		//bank.addAccount("Deac","Dan",20,200, "Spending");
		//System.out.println(bank.toString());
		//bank.depozitaSuma(2, 100);
		

		System.out.println(bank.toString());
		
		//bank.retrageSuma(2, 100);
		
		//System.out.println(bank.toString());
		
		//System.out.println(bank.interogareSold(1));
		
		//bank.unEmpowerPerson(1, p2);
		
		//bank.transferMoney(2, 1, 100);
		System.out.println(bank.toString());
	}
}
