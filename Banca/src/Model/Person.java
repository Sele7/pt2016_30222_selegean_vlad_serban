package Model;

import java.io.Serializable;

import java.io.Serializable;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Person.
 */
public class Person extends Observer implements Serializable  {
	
	/** The sur name. */
	private String surName;
	
	/** The name. */
	private String name;
	
	/** The age. */
	private int age;
	
	/** The subject. */
	private Account subject;
	
	/**
	 * Instantiates a new person.
	 *
	 * @param surName the sur name
	 * @param name the name
	 * @param age the age
	 */
	public Person(String surName, String name, int age) {
		super();
		this.surName = surName;
		this.name = name;
		this.age = age;
		
	}
	
	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(Account subject){
		this.subject = subject;
		this.subject.attach(this);
	}

	/**
	 * Gets the sur name.
	 *
	 * @return the sur name
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * Sets the sur name.
	 *
	 * @param surName the new sur name
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Sets the age.
	 *
	 * @param age the new age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/* (non-Javadoc)
	 * @see Model.Observer#update()
	 */
	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println( " !Change! Sum Changed To " +  subject.getSum());
	}
	
	
	

}
