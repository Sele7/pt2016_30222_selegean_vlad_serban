package Model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class SavingAccount.
 */
public class SavingAccount extends Account implements Serializable{

	/**
	 * Instantiates a new saving account.
	 *
	 * @param number the number
	 * @param sum the sum
	 * @param person the person
	 */
	public SavingAccount(int number, int sum, Person person) {
		super(number, sum, person);
		this.type = "Saving";
		// TODO Auto-generated constructor stub
	}

}
