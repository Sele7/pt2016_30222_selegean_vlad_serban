package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Product;
import java.awt.Dimension;

// TODO: Auto-generated Javadoc
/**
 * The Class GUI.
 */
public class GUI implements Serializable{

	/** The frame. */
	public static JFrame frame;
	
	/** The field pret. */
	private JTextField fieldPret;
	
	/** The field cantitate. */
	private JTextField fieldCantitate;
	
	/** Tabelul 1 pt produse */
	private JTable table;
	
	/** The table head. */
	private String tableHead[] = { "ID", "Product name", "Quantity" , "Price" };
	
	/** The price range. */
	private String[] priceRange = {"Any price", "<50","50-100","100-200","200-500","500-1000",">1000"};
	
	/** The categorii. */
	private String[] categorii = {"Any type","Drinks", "Food","Electronics","Consumers","Gaming","Apparel","Health"};

	/** The combo box produse. */
	JComboBox<String> comboBoxProduse = new JComboBox<String>();
	
	/** The scroll pane. */
	JScrollPane scrollPane = new JScrollPane();
	 
 	/** The dtm. */
 	DefaultTableModel dtm = new DefaultTableModel(0, 0);
	 
 	/** The btn order. */
 	JButton btnOrder = new JButton("Order");
	 
	 
	 /** Tabelul 2 pt orders */
 	private JTable table_1;
	 
 	/** The scroll pane_1. */
 	JScrollPane scrollPane_1 = new JScrollPane();
	 
 	/** The dtm2. */
 	DefaultTableModel dtm2 = new DefaultTableModel(0, 0);
	 
	 /** Tabelul 3 pt Customers. */
 	private JTable table_2;
	 
 	/** The scroll pane_2. */
 	JScrollPane scrollPane_2 = new JScrollPane();
	 
 	/** The dtm3. */
 	DefaultTableModel dtm3 = new DefaultTableModel(0, 0);
	 
	 /** The btn add product. */
 	JButton btnAddProduct = new JButton("Add Product");
	 
 	/** The btn delete product. */
 	JButton btnDeleteProduct = new JButton("Delete Product");
	 
 	/** The btn edit product. */
 	JButton btnEditProduct = new JButton("Edit Product");
	 
	 /** The btn new button_1. */
 	JButton btnNewButton_1 = new JButton("Add Client");
	 
 	/** The btn delete client. */
 	JButton btnDeleteClient = new JButton("Delete Client");
	 
 	/** The btn edit client. */
 	JButton btnEditClient = new JButton("Edit Client");
	 
	 
	 /** The combo box_1. Pentru boxul de Categorii */
 	JComboBox<String> comboBox_1 = new JComboBox<String>(categorii);
	 
 	/** The combo box_2. pentru boxul de price range*/
 	JComboBox<String> comboBox_2 = new JComboBox<String>(priceRange);
	 
 	/** butonul pentru Search */
 	JButton btnNewButton = new JButton("Search");
	 
 	/** Butonul pentru clear. */
 	JButton btnClear = new JButton("Clear");
	 
	 /**  Tabelul 4 pentru Search afisand produsele filtrate*/
 	private JTable table_3;
	 
 	/** The scroll pane_3. */
 	private  JScrollPane scrollPane_3 = new JScrollPane();
	 
 	/** The dtm4. */
 	DefaultTableModel dtm4 = new DefaultTableModel(0, 0);
	 
 	/** The btn export. */
 	private  JButton btnExport = new JButton("Export");
	 
	 /** The btn serialize. */
 	JButton btnSerialize = new JButton("Serialize");
	 
 	/** The btn cancelord. */
 	private  JButton btnCancelord = new JButton("Cancel");
	 
	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
	
		
		//Object[][] data = {{1, "Smith", 5, 10}};
		frame = new JFrame("WareHouse");
		frame.setBounds(100, 50, 636, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(20, 26, 590, 560);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Order", null, panel, null);
		panel.setLayout(null);
		
		
		comboBoxProduse.setBounds(10, 73, 133, 20);
		panel.add(comboBoxProduse);
		
		fieldPret = new JTextField();
		fieldPret.setEditable(false);
		fieldPret.setBounds(178, 73, 86, 20);
		panel.add(fieldPret);
		fieldPret.setColumns(10);
		
		fieldCantitate = new JTextField();
		fieldCantitate.setBounds(311, 73, 86, 20);
		panel.add(fieldCantitate);
		fieldCantitate.setColumns(10);
		
		JLabel lblProdus = new JLabel("Produs");
		lblProdus.setBounds(49, 48, 46, 14);
		panel.add(lblProdus);
		
		JLabel lblPret = new JLabel("Pret");
		lblPret.setBounds(199, 48, 46, 14);
		panel.add(lblPret);
		
		JLabel lblCantitate = new JLabel("Cantitate");
		lblCantitate.setBounds(330, 48, 86, 14);
		panel.add(lblCantitate);
		
		
		btnOrder.setBounds(175, 151, 89, 23);
		panel.add(btnOrder);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("View Orders", null, panel_1, null);
		panel_1.setLayout(null);
		
		
		
		scrollPane_1.setBounds(10, 11, 418, 510);
		panel_1.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.setModel(dtm2);
		scrollPane_1.setViewportView(table_1);
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnExport.setBounds(464, 42, 89, 23);
		
		panel_1.add(btnExport);
		btnCancelord.setBounds(464, 105, 89, 23);
		
		panel_1.add(btnCancelord);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Produse", null, panel_2, null);
		panel_2.setLayout(null);
		
		
		scrollPane.setBounds(10, 26, 418, 495);
		panel_2.add(scrollPane);
		
		table = new JTable();
		table.setModel(dtm);
		scrollPane.setViewportView(table);
		
		
		btnAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddProduct.setBounds(448, 36, 114, 23);
		panel_2.add(btnAddProduct);
		
		
		btnDeleteProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteProduct.setBounds(448, 106, 114, 23);
		panel_2.add(btnDeleteProduct);
		
		
		btnEditProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEditProduct.setBounds(448, 181, 114, 23);
		panel_2.add(btnEditProduct);
		
		JPanel panel_5 = new JPanel();
		tabbedPane.addTab("Clienti", null, panel_5, null);
		panel_5.setLayout(null);
		
		
		scrollPane_2.setBounds(10, 11, 416, 510);
		panel_5.add(scrollPane_2);
		
		table_2 = new JTable();
		table_2.setModel(dtm3);
		scrollPane_2.setViewportView(table_2);
		
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(436, 22, 139, 23);
		panel_5.add(btnNewButton_1);
		
		
		btnDeleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteClient.setBounds(436, 87, 139, 23);
		panel_5.add(btnDeleteClient);
		
		btnEditClient.setBounds(436, 155, 139, 23);
		panel_5.add(btnEditClient);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Search", null, panel_3, null);
		panel_3.setLayout(null);
		
		
		comboBox_1.setBounds(10, 77, 118, 20);
		panel_3.add(comboBox_1);
		
		comboBox_2.setBounds(160, 77, 91, 20);
		panel_3.add(comboBox_2);
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(436, 96, 89, 23);
		panel_3.add(btnNewButton);
		
		btnClear.setBounds(436, 48, 89, 23);
		panel_3.add(btnClear);
		
		JLabel lblCategorie = new JLabel("Categorie");
		lblCategorie.setBounds(41, 52, 69, 14);
		panel_3.add(lblCategorie);
		
		JLabel lblPret_1 = new JLabel("Pret <>");
		lblPret_1.setBounds(182, 52, 69, 14);
		panel_3.add(lblPret_1);
		scrollPane_3.setBounds(41, 118, 370, 403);
		
		panel_3.add(scrollPane_3);
		table_3 = new JTable();
		table_3.setModel(dtm4);
		scrollPane_3.setViewportView(table_3);
		
		JPanel panel_4 = new JPanel();
		tabbedPane.addTab("Save", null, panel_4, null);
		panel_4.setLayout(null);
		
		JLabel lblSaveYourDatas = new JLabel("Save Your Data(Serialize)");
		lblSaveYourDatas.setBounds(190, 79, 262, 14);
		panel_4.add(lblSaveYourDatas);
		
		
		btnSerialize.setBounds(190, 104, 107, 23);
		panel_4.add(btnSerialize);
	}
	
	/**
	 * Gets the frame.
	 *
	 * @return the frame
	 */
	public JFrame getFrame(){
		return frame;
	}
	
	/**
	 * Sets the frame.
	 *
	 * @param frame the new frame
	 */
	public void setFrame(JFrame frame){
		this.frame=frame;
	}
	
	/**
	 * Gets the combo box produse.
	 *
	 * @return the combo box produse
	 */
	// pt Order
	public JComboBox<String> getComboBoxProduse()
	{
		return comboBoxProduse;
	}
	
	/**
	 * Gets the field price.
	 *
	 * @return the field price
	 */
	public JTextField getFieldPrice() {
		return fieldPret;
	}
	
	/**
	 * Gets the field cantitate.
	 *
	 * @return the field cantitate
	 */
	public JTextField getFieldCantitate() {
		return fieldCantitate;
	}
	
	/**
	 * Umple comboBoxul de produse , cu produsele din depozit
	 *
	 * @param produse the produse
	 */
	public void fillComboBoxProduse(List<Product> produse){
		for(int i = 0;i < produse.size();i++)
			comboBoxProduse.addItem(produse.get(i).getName());
	}
	
	/**
	 * Combo listener.
	 *
	 * @param listenForProduse the listen for produse
	 */
	public void comboListener(ActionListener listenForProduse){
		comboBoxProduse.addActionListener(listenForProduse);
		}
	
	/**
	 * Order listener.
	 *
	 * @param listenForOrder the listen for order
	 */
	public void orderListener(ActionListener listenForOrder){
		btnOrder.addActionListener(listenForOrder);
		}
	
	/**
	 * Export listener.
	 *
	 * @param listenForExport the listen for export
	 */
	public void exportListener(ActionListener listenForExport){
		btnExport.addActionListener(listenForExport);
		}
	
	/**
	 * Btn serializare.
	 *
	 * @param listenForSerializare the listen for serializare
	 */
	public void btnSerializare(ActionListener listenForSerializare){
		btnSerialize.addActionListener(listenForSerializare);
		}
	
	/**
	 * Cancel ord.
	 *
	 * @param listenForCancel the listen for cancel
	 */
	public void cancelOrd(ActionListener listenForCancel){
		btnCancelord.addActionListener(listenForCancel);
	}
	//-----------
	
	/**
	 * Sets the table.
	 *
	 * @param table the new table
	 */
	//pt TABEL
	public void setTable(JTable table)
	{
		scrollPane.setViewportView(table);
	}
	
	/**
	 * Sets the table2.
	 *
	 * @param table_1 the new table2
	 */
	public void setTable2(JTable table_1)
	{
		scrollPane_1.setViewportView(table_1);
	}
	
	/**
	 * Gets the table2.
	 *
	 * @return the table2
	 */
	public JTable getTable2()
	{
		return table;
	}
	
	/**
	 * Sets the table3.
	 *
	 * @param table_2 the new table3
	 */
	public void setTable3(JTable table_2)
	{
		scrollPane_2.setViewportView(table_2);
	}
	
	/**
	 * Sets the table4.
	 *
	 * @param table_3 the new table4
	 */
	public void setTable4(JTable table_3)
	{
		scrollPane_3.setViewportView(table_3);
	}
	//-----------------
	
	/**
	 * Adds the product.
	 *
	 * @param listenForAdd the listen for add
	 */
	//pt Produse
	public void addProduct(ActionListener listenForAdd){
		btnAddProduct.addActionListener(listenForAdd);
		}
	
	/**
	 * Delete product.
	 *
	 * @param listenForDelete the listen for delete
	 */
	public void deleteProduct(ActionListener listenForDelete){
		btnDeleteProduct.addActionListener(listenForDelete);
		}
	
	/**
	 * Edits the product.
	 *
	 * @param listenForEdit the listen for edit
	 */
	public void editProduct(ActionListener listenForEdit){
		btnEditProduct.addActionListener(listenForEdit);
		}
	
	//------
	
	/**
	 * Adds the client.
	 *
	 * @param listenForAddC the listen for add c
	 */
	//pt Clienti
	public void addClient(ActionListener listenForAddC){
		btnNewButton_1.addActionListener(listenForAddC);
		}
	
	/**
	 * Delete client.
	 *
	 * @param listenForDeleteC the listen for delete c
	 */
	public void deleteClient(ActionListener listenForDeleteC){
		btnDeleteClient.addActionListener(listenForDeleteC);
		}
	
	/**
	 * Edits the client.
	 *
	 * @param listenForEditC the listen for edit c
	 */
	public void editClient(ActionListener listenForEditC){
		btnEditClient.addActionListener(listenForEditC);
		}
	
	//--------
	
	/**
	 * Clear t.
	 *
	 * @param listenForClear the listen for clear
	 */
	//pt Search
	public void clearT(ActionListener listenForClear){
		btnClear.addActionListener(listenForClear);
		}
	
	/**
	 * Search t.
	 *
	 * @param listenForSearch the listen for search
	 */
	public void searchT(ActionListener listenForSearch){
		btnNewButton.addActionListener(listenForSearch);
		}
	
	/**
	 * Gets the combo box search.
	 *
	 * @return the combo box search
	 */
	public JComboBox<String> getComboBoxSearch()
	{
		return comboBox_1;
	}
	
	/**
	 * Gets the combo box price.
	 *
	 * @return the combo box price
	 */
	public JComboBox<String> getComboBoxPrice()
	{
		return comboBox_2;
	}
}	
