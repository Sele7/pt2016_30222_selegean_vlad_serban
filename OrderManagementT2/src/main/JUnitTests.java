package main;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import controller.Controller;
import model.Customer;
import model.Model;
import model.NotAvailableException;
import model.OPDept;
import model.OverStockException;
import model.Product;
import model.WareHouse;
import view.GUI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

// TODO: Auto-generated Javadoc
/**
 * The Class JUnitTests.
 */
public class JUnitTests {
	
	/** The warehouse. */
	WareHouse warehouse = new WareHouse();
	
	/** The op dept. */
	OPDept opDept = new OPDept(warehouse);
	
	/** The model. */
	Model model = new Model(warehouse,opDept);
	
	/** The view. */
	GUI view = new GUI();
	
	/** The ctrl. */
	Controller ctrl = new Controller(view, model);
	
	
	/**
	 * Verifica adaugare prod.
	 *
	 * @return true, if successful
	 */
	public boolean verificaAdaugareProd(){
		boolean OK = false;
		Product p1 = new Product("GANJA", 1, 5, 50,"Food");
		try {
			warehouse.addProduct(p1);
		} catch (OverStockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(warehouse.getDepozit().containsValue(p1))
			OK = true;
		else
			OK=false;
		
		return OK;
	}
	
	/**
	 * Verifica editare prod.
	 *
	 * @return true, if successful
	 */
	public boolean verificaEditareProd(){
		boolean OK = false;
		Product p1 = new Product("GANJA", 1, 5, 50,"Food");
		try {
			warehouse.addProduct(p1);
		} catch (OverStockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			warehouse.modifyProduct(1, 60);
		} catch (NotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(warehouse.getDepozit());
		if(warehouse.getDepozit().get(1).getPrice()== 60.0)
			OK = true;
		else
			OK=false;
		
		return OK;
	}
	
	

	/**
	 * Verifica remove prod.
	 *
	 * @return true, if successful
	 */
	public boolean verificaRemoveProd(){
		boolean OK = false;
		Product p1 = new Product("GANJA", 1, 5, 50,"Food");
		try {
			warehouse.addProduct(p1);
		} catch (OverStockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(warehouse.getDepozit());
		try {
			warehouse.removeProduct(1);
		} catch (NotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(warehouse.getDepozit());
		if(warehouse.getDepozit().containsValue(p1))
			OK = false;
		else
			OK= true;
		
		return OK;
	}
	
	/**
	 * Verifica adaugare clienti.
	 *
	 * @return true, if successful
	 */
	public boolean verificaAdaugareClienti(){
		boolean OK = false;
		Customer c1 = new Customer(1,"Vlad","S","v@y.com");
		opDept.getClienti().add(c1);
		System.out.println(opDept.getClienti());
		if(opDept.getClient1(1).getNume().equals("Vlad"))
			OK = true;
		else
			OK=false;
		
		return OK;
	}
	
	/**
	 * Verifica edit clienti.
	 *
	 * @return true, if successful
	 */
	public boolean verificaEditClienti(){
		boolean OK = false;
		Customer c1 = new Customer(1,"Vlad","S","v@y.com");
		opDept.getClienti().add(c1);
		System.out.println(opDept.getClienti());
		
		opDept.editClient(1, "Dulau", "Tudor", "DT@y.com");
		
		System.out.println(opDept.getClienti());
		if(opDept.getClient1(1).getNume().equals("Dulau"))
			OK = true;
		else
			OK=false;
		
		return OK;
	}
	
	/**
	 * Verifica remove clienti.
	 *
	 * @return true, if successful
	 */
	public boolean verificaRemoveClienti(){
		boolean OK = false;
		Customer c1 = new Customer(1,"Vlad","S","v@y.com");
		opDept.getClienti().add(c1);
		System.out.println(opDept.getClienti());
		
		opDept.removeClient(1);
		
		System.out.println(opDept.getClienti());
		if(opDept.getClienti().size() == 0)
			OK = true;
		else
			OK=false;
		
		return OK;
	}
	
	/**
	 * Test.
	 */
	//Metoda de testing
	@Test
	public void test(){
	//assertTrue(verificaAdaugareProd());
	//assertTrue(verificaEditareProd());
	//assertTrue(verificaRemoveProd());
	//assertTrue(verificaAdaugareClienti());
	//assertTrue(verificaEditClienti());
	//assertTrue(verificaRemoveClienti());
	}

}
