package main;

import java.sql.Timestamp;

import controller.Controller;
import model.Customer;
import model.Model;
import model.NotAvailableException;
import model.OPDept;
import model.Order;
import model.OverStockException;
import model.Product;
import model.UnderStockException;
import model.WareHouse;
import view.GUI;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args){
		
		/** Initializarea claselor necesare si vizibilitate frame-ului*/
		WareHouse warehouse = new WareHouse();
		OPDept opDept = new OPDept(warehouse);
		Model model = new Model(warehouse,opDept);
		GUI view = new GUI();
		
		GUI.frame.setVisible(true);
	/*
		Product p1 = new Product("Pantofi", 1, 5, 150,"Apparel");
		Product p2 = new Product("Cutit", 2, 10, 50,"Drinks");
		
		Customer c = new Customer(1,"Vlad","S","v@y.com");
		Customer c2 = new Customer(2,"VladS","SS","vS@y.com");

		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		
		//Order o = new Order(1,c,p1,1,timeStamp,150.0);
		//Order o2 = new Order(2,c,p2,1,timeStamp,50.0);
		
		
		
		
		
	
		
		
			try {
				warehouse.addProduct(p1);
				warehouse.addProduct(p2);
				//System.out.println(warehouse.getDepozit().get(2).getId());
				
				//opDept.gettComenzi().put((long) 1, o);
				//opDept.gettComenzi().put((long) 2, o2);
				
				opDept.getClienti().add(c);
				opDept.getClienti().add(c2);
				
//				try {
//					opDept.placeOrder(o);
//				} catch (NotAvailableException | UnderStockException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
			} catch (OverStockException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	*/		
			
		/** Metodele folosite pentru deserializare */
		model.getWarehouse().loadWarehouse();
		model.getOpDept().loadCustomers();
		model.getOpDept().loadOrders();	
		
		Controller ctrl = new Controller(view, model);
	}
	
}
