package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Customer;
import model.Model;
import model.NotAvailableException;
import model.Order;
import model.OverStockException;
import model.Product;
import model.UnderStockException;
import view.GUI;

// TODO: Auto-generated Javadoc
/**
 * The Class Controller.
 */
public class Controller implements Serializable {
	
	/** The view. */
	private GUI view;
	
	/** The model. */
	private Model model;
	
	
	
	
	/**
	 * Instantiates a new controller.
	 * Instantiam clasele de actionlistener
	 * @param view the view
	 * @param model the model
	 */
	public Controller(GUI view, Model model) {
		super();
		this.view = view;
		this.model = model;
		
		this.view.comboListener(new comboListener());
		this.view.orderListener(new orderListener());
		this.view.addProduct(new addProduct());
		this.view.deleteProduct(new deleteProduct());
		this.view.editProduct(new editProduct());
		this.view.addClient(new addClient());
		this.view.deleteClient(new deleteClient());
		this.view.editClient(new editClient());
		this.view.searchT(new searchT());
		this.view.clearT(new clearT());
		this.view.exportListener(new export());
		this.view.btnSerializare(new btnSerializare());
		this.view.cancelOrd(new cancelOrd());
		
		listProducts();
		listOrders();
		listCustomers();
		boxProducts();
	}





	/**
	 * Listeaza produsele in tabelul produse.
	 */
	private void listProducts() {
		TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
		fillTable.insertRows();
		view.setTable(fillTable.getTable());
	}
	
	/**
	 * Listeaza comenzile in tabelul de orders.
	 */
	private void listOrders() {
		TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
		fillTable.insertRows2();
		view.setTable2(fillTable.getTable2());
	}
	
	/**
	 * Listeaza clientii in tabelul customers.
	 */
	private void listCustomers() {
		TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
		fillTable.insertRows3();
		view.setTable3(fillTable.getTable3());
	}
	
//	private void listSearch() {
//		TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
//		String s1 = view.getComboBoxSearch().getSelectedItem().toString();
//		fillTable.inserFilteredtRows(s1,"Any Price");
//		view.setTable4(fillTable.getTable4());
//	}
	
	/**
 * Metoda umple comboBoxul cu produsele din depozit folosind metoda din GUI
 */
private void boxProducts(){
		List<Product> produse = model.getWarehouse().productsList();
		view.fillComboBoxProduse(produse);
		
	}
	
	/**
	 * Metoda seteaza fieldTextul specific label-ului Price cu pretul corespunzator fiecarui produs
	 *
	 * @see comboEvent
	 */
	private class comboListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			view.getFieldPrice().setText(String.valueOf(model.getWarehouse().getProductPrice(view.getComboBoxProduse().getSelectedItem().toString())));
		}
		
	}
	
	/**
	 *	Metoda este folosita pentru a face o comanda , apare un doalog box in care se introduc datele necesare si se adauga in tabelul orders
	 * se foloseste metoda placeOrder din OPDept
	 * @see orderEvent
	 */
	private class orderListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String cant = view.getFieldCantitate().getText().toString();
			Double cant2 = Double.parseDouble(cant);
			Integer cant3 = (int) (cant2*1.0);
			//System.out.println(cant2);
			String s = view.getFieldPrice().getText().toString();
			Double s2 = Double.parseDouble(s);
			//System.out.println(s2);
			double total = cant2 * s2;
			
			JTextField id = new JTextField();
			JTextField idC = new JTextField();
			JTextField idP = new JTextField();
			//JTextField cantit = new JTextField();
			Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
			
			Object[] message = {
                    "*fill all fields\nOrder ID", id,
                    "Client ID:", idC
                    
            };
			//"Product ID:", idP
			int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Add Order", JOptionPane.OK_CANCEL_OPTION);
            
            if (option == JOptionPane.OK_OPTION) {
            	
            	int idOr = Integer.parseInt(id.getText());
            	int idCu = Integer.parseInt(idC.getText());
            	///int idPr = Integer.parseInt(idP.getText());
            	//int cantitate = Integer.parseInt(cantit.getText());

            	
            	Order o = new Order(idOr,model.getOpDept().getClient1(idCu),model.getWarehouse().getProduct2(view.getComboBoxProduse().getSelectedItem().toString()),cant3,timeStamp,total);
            	

			
            	
            	try {
					model.getOpDept().placeOrder(o);
				} catch (NotAvailableException | UnderStockException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	model.getOpDept().gettComenzi().put((long) idOr, o);
            	listOrders();
            	listProducts();
            }

		}
		
	}
	
	/**
	 * Metoda adauga un produs in depozit si in tabelul cu produse - apare un dialog care cere datele necesare
	 * Adaugarea se face prin metoda addProduct din Warehouse
	 */
	private class addProduct implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			 	JTextField id = new JTextField();
	            JTextField name = new JTextField();
	            JTextField price = new JTextField();
	            JTextField quantity = new JTextField();
	            JTextField category = new JTextField();
	            
	            Object[] message = {
	                    "*fill all fields\nProduct ID", id,
	                    "New name:", name,
	                    "New price:", price,
	                    "New quantity", quantity,
	                    "New Category", category
	            };
			
	            int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Add Product", JOptionPane.OK_CANCEL_OPTION);
	            
	            if (option == JOptionPane.OK_OPTION) {
	            	String name2 = name.getText();
	            	int id2 = Integer.parseInt(id.getText());
	            	double pret = Double.parseDouble(price.getText());
	            	int cant = Integer.parseInt(quantity.getText());
	            	String cat = category.getText();
	            	
	            	Product p1 = new Product(name2,id2,cant,pret,cat);
	            	
	            	
	            	try {
						model.getWarehouse().addProduct(p1);
					} catch (OverStockException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            	
	            	listProducts();
	            }
	            
	            
	            
		}
		
	}
	
	
	/**
	 * Sterge un produs din depozit si din tabelul de produse. - dialog pentru datele necesare
	 * foloseste metoda removeProduct din Warehouse
	 */
	private class deleteProduct implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
			
			Object[] message = {
                    "*fill all fields\nProduct ID", id};
			
			
			int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Delete Product", JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {
         
            	int id2 = Integer.parseInt(id.getText());
 
					try {
						model.getWarehouse().removeProduct(id2);
					} catch (NotAvailableException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
            	
            	listProducts();
            }
		}
		
		
	}
	
	/**
	 * Editarea produsului , modificarea pretului mai exact - dialog pentru datele necesare
	 * se foloseste metoda modifyProduct din Warehouse
	 */
	private class editProduct implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
			JTextField price = new JTextField();
			
			Object[] message = {
                    "*fill all fields\nProduct ID", id,
                    "New price:", price
            };
			
			int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Edit Product", JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {
		         
            	int id2 = Integer.parseInt(id.getText());
            	int pret = Integer.parseInt(price.getText());

				try {
					model.getWarehouse().modifyProduct(id2, pret);
				} catch (NotAvailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

            	listProducts();
            }
		}
		
	}
	
	/**
	 * Adauga un client la lista de clienti si in tabelul clienti  - dialog pentru datele necesare
	 * 
	 */
	private class addClient implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
            JTextField name = new JTextField();
            JTextField pname = new JTextField();
            JTextField email = new JTextField();
         
            
            Object[] message = {
                    "*fill all fields\nClient ID", id,
                    "New name:", name,
                    "New Sname:", pname,
                    "New email", email
            };
		
            int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Add Client", JOptionPane.OK_CANCEL_OPTION);
            
            if (option == JOptionPane.OK_OPTION) {
            	String name2 = name.getText();
            	String name3 = pname.getText();
            	int id2 = Integer.parseInt(id.getText());
            	String email2 = email.getText();
            	
            	
            	Customer c = new Customer(id2,name2,name3,email2);
            	
            	
				model.getOpDept().getClienti().add(c);
			
            	
				listCustomers();
            }
			
		}
		
	}
	
	
	/**
	 * Sterge clienti din lista de clienti si din tabel  - dialog pentru datele necesare
	 */
	private class deleteClient implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
			
			Object[] message = {
                    "*fill all fields\nClient ID", id};
			
			int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Delete Client", JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {

            	int id2 = Integer.parseInt(id.getText());

   
				model.getOpDept().removeClient(id2);
				listCustomers();
            }
			
		}
		
	}
	
	/**
	 * Editeaza datele clientului din lista si tabel  - dialog pentru datele necesare
	 * se foloseste metoda editClient din OPDept
	 */
	private class editClient implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
            JTextField name = new JTextField();
            JTextField pname = new JTextField();
            JTextField email = new JTextField();
         
            
            Object[] message = {
                    "*fill all fields\nClient ID", id,
                    "New name:", name,
                    "New Sname:", pname,
                    "New email", email
            };
		
            int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Add Client", JOptionPane.OK_CANCEL_OPTION);
            
            if (option == JOptionPane.OK_OPTION) {
            	String name2 = name.getText();
            	String name3 = pname.getText();
            	int id2 = Integer.parseInt(id.getText());
            	String email2 = email.getText();
  
				model.getOpDept().editClient(id2, name2, name3, email2);

				listCustomers();
            }
		}
		
	}
	
	
	/**
	 * folosita pentru umplerea tabelului specific filtrarii in functie de filtre
	 */
	private class searchT implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
			String s1 = view.getComboBoxSearch().getSelectedItem().toString();
			String s2 = view.getComboBoxPrice().getSelectedItem().toString();
			fillTable.inserFilteredtRows(s1,s2);
			view.setTable4(fillTable.getTable4());
		}
		
	}
	
	/**
	 * sterge datele din tabelul de filtre pt o alta cautare
	 * foloseste metoda reset() din view
	 */
	private class clearT implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
			fillTable.reset();
			view.setTable4(fillTable.getTable4());
		}
		
	}
	
	/**
	 * metoda exporta datele tabelului orders intr un .txt ca bon fiscal
	 * 
	 */
	private class export implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			TableFill fillTable = new TableFill(model.getWarehouse(),model.getOpDept());
			fillTable.insertRows2();
			view.setTable2(fillTable.getTable2());
			
			try{
				//the file path
				File file = new File("BonTaxa.txt");
				//if the file not exist create one
				 if(!file.exists()){
	                   file.createNewFile();
	               }
				 FileWriter fw = new FileWriter(file.getAbsoluteFile());
				 BufferedWriter bw = new BufferedWriter(fw);
				//loop for jtable rows
				 for(int i = 0; i < fillTable.getTable2().getRowCount(); i++){
					 
					//loop for jtable column
					 for(int j = 0; j < fillTable.getTable2().getColumnCount(); j++){
						 bw.write(fillTable.getTable2().getModel().getValueAt(i, j)+" ");
						 
					 }
					 bw.newLine();
				 }
				 bw.write("\n");
				//close BufferedWriter
	            bw.close();
	           //close FileWriter 
	           fw.close();
	           JOptionPane.showMessageDialog(null, "Data Exported");
			}catch(Exception ex){
				ex.printStackTrace();
            }
			}
		}
		
	/**
	 * Metoda ce apeleaza metodele de serializare a datelor
	 */
	private class btnSerializare implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			model.getWarehouse().storeWarehouse();
			model.getOpDept().storeCustomers();
			model.getOpDept().storeOrders();
			JOptionPane.showMessageDialog(null, "Data Serialized");
		}
		
	}
	
	/**
	 * Metoda anuleaza - sterge o comanda care ar trebui executata din TreeMap si din tabel
	 * foloseste removeOrder din OPdept
	 */
	private class cancelOrd implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField id = new JTextField();
			
			Object[] message = {
                    "*fill all fields\nOrder ID", id,
            };
		
            int option = JOptionPane.showConfirmDialog(GUI.frame, message, "Cancel Order", JOptionPane.OK_CANCEL_OPTION);
			
            if (option == JOptionPane.OK_OPTION) {

            	
            	 long id2 = (long) Double.parseDouble(id.getText());
  
            	try {
					model.getOpDept().removeOrder(id2);
				} catch (NotAvailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				

            	listOrders();
            }
			
		}
		
	}
}
