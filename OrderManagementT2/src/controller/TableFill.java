package controller;

import java.util.TreeMap;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.OPDept;
import model.Order;
import model.Product;
import model.WareHouse;
import view.GUI;

// TODO: Auto-generated Javadoc
/**
 * The Class TableFill.
 */
public class TableFill {
	
	/** The t map. */
	private TreeMap<Integer,Product> tMap = new TreeMap<Integer,Product>();
	
	/** The t comenzi. */
	TreeMap<Long,Order> tComenzi = new TreeMap<Long, Order>();
	
	/** The warehouse. */
	private WareHouse warehouse;
	
	/** The op dept. */
	private OPDept opDept;
	
	/** The active. */
	private boolean active = false;
	
	/** The table. */
	private JTable table = new JTable();
	
	/** The table2. */
	private JTable table2 = new JTable();
	
	/** The table3. */
	private JTable table3 = new JTable();
	
	/** The table4. */
	private JTable table4 = new JTable();
	
	/** The view. */
	private GUI view;
	
	/** The dtm. */
	DefaultTableModel dtm = new DefaultTableModel(0, 0);
	
	/** The table head. */
	private String tableHead[] = { "ID", "Product Name", "Category" , "Price","Cantitate"  };
	
	/** The dtm2. */
	DefaultTableModel dtm2 = new DefaultTableModel(0, 0);
	
	/** The table head2. */
	private String tableHead2[] = { "ID", "Customer", "Produs" , "Cantitate","Time","Total"  };
	
	/** The dtm3. */
	DefaultTableModel dtm3 = new DefaultTableModel(0, 0);
	
	/** The table head3. */
	private String tableHead3[] = { "ID", "Nume", "Prenume" , "Email" };
	
	/** The dtm4. */
	DefaultTableModel dtm4 = new DefaultTableModel(0, 0);
	
	/** The table head4. */
	private String tableHead4[] = { "ID", "Product Name", "Category" , "Price","Cantitate"  };
	
/**
 * Instantiates a new table fill.
 */
public TableFill(){
		
	}
	
	
	/**
	 * Instantiates a new table fill.
	 *
	 * @param warehouse the warehouse
	 * @param opDept the op dept
	 */
	public TableFill(WareHouse warehouse,OPDept opDept) {

		this.warehouse = warehouse;
		this.opDept = opDept;

	}
	
/**
 * Insereaza rows pt tabelul de produse.
 */
public void insertRows() {
		
		//table = view.getTable();
		dtm.setColumnIdentifiers(tableHead);
		table.setModel(dtm);
		dtm.setRowCount(0);
		for (Map.Entry<Integer, Product> entry : warehouse.getDepozit().entrySet()) {
			dtm.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
					entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
			//table.setModel(table);
		}
		
	}

/**
 * Insereaza rows pt tabelul de orders.
 */
public void insertRows2() {
	
	//table = view.getTable();
	if((opDept.getNo1() || opDept.getNo2() ) == true)
		return;
	else
	{
	dtm2.setColumnIdentifiers(tableHead2);
	table2.setModel(dtm2);
	dtm2.setRowCount(0);
	for (Map.Entry<Long,Order> entry : opDept.gettComenzi().entrySet()) {
		dtm2.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getCustomer().toString(),
				entry.getValue().getProdus().getName(),entry.getValue().getCantitate(),entry.getValue().getTimeStamp(),entry.getValue().getTotal()});
		//table.setModel(table);
	}
	}
}

/**
 * Insereaza rows pt tabelul de clienti/customers.
 */
public void insertRows3() {
	
	//table = view.getTable();
	dtm3.setColumnIdentifiers(tableHead3);
	table3.setModel(dtm3);
	dtm3.setRowCount(0);
	for(int i = 0 ; i<opDept.getClienti().size();i++)
		dtm3.addRow(new Object[]{opDept.getClienti().get(i).getId(),opDept.getClienti().get(i).getNume(),opDept.getClienti().get(i).getPrenume(),
				opDept.getClienti().get(i).getEmail()});
	
//	for (Map.Entry<Long,Order> entry : opDept.gettComenzi().entrySet()) {
//		dtm2.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getCustomer().toString(),
//				entry.getValue().getProdus().getName(),entry.getValue().getCantitate(),entry.getValue().getTimeStamp()});
		//table.setModel(table);
//	}
	
}

/**
 * Insereaza rows filtrate pentru tabelul search.
 *
 * @param s1 the s1
 * @param s2 the s2
 */
public void inserFilteredtRows(String s1, String s2) {

	dtm4.setColumnIdentifiers(tableHead4);
	table4.setModel(dtm4);
	dtm4.setRowCount(0);
	//daca se cauta orice categorie si orice price range
	if ((s1.equals("Any type") && s2.equals("Any price")) == true) {
		for (Map.Entry<Integer, Product> entry : warehouse.getDepozit().entrySet()) {
			dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
					entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
			
		}
		
	} else {//daca se cauta pentru o categorie anume dar orice price range
		if ((!s1.equals("Any type") && s2.equals("Any price")) == true) {
			for (Map.Entry<Integer, Product> entry : warehouse.getDepozit().entrySet()) {
				if (entry.getValue().getCategorie().equals(s1)) {
					dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
					
				}
			}
		}
		//daca se cauta o categorie anume si un anumit price range
		if (!s1.equals("Any type") && !s2.equals("Any price")) {
            for (Map.Entry<Integer, Product> entry : warehouse.getDepozit().entrySet()) {
                if (entry.getValue().getCategorie().equals(s1)) {
                    switch (s2) {
                    case "<50":
                        if (entry.getValue().getPrice() < 50) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    case "50-100":
                        if (entry.getValue().getPrice() >= 50 && entry.getValue().getPrice() < 100) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    case "100-200":
                        if (entry.getValue().getPrice() >= 100 && entry.getValue().getPrice() < 200) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    case "200-500":
                        if (entry.getValue().getPrice() >= 200 && entry.getValue().getPrice() < 500) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    case "500-1000":
                        if (entry.getValue().getPrice() >= 500 && entry.getValue().getPrice() < 1000) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    case ">1000":
                        if (entry.getValue().getPrice() >= 1000) {
                        	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
        							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                        }
                        break;
                    default:
                    }
                }
            }
        }
		//daca se cauta in orice categorie dar range de pret ales de utilizator
		if (s1.equals("Any type") && !s2.equals("Any price")) {
            for (Map.Entry<Integer, Product> entry : warehouse.getDepozit().entrySet()) {
                switch (s2) {
                case "<50":
                    if (entry.getValue().getPrice() < 50) {
                    	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                case "50-100":
                    if (entry.getValue().getPrice() >= 50 && entry.getValue().getPrice() < 100) {
                    	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                case "100-200":
                    if (entry.getValue().getPrice() >= 100 && entry.getValue().getPrice() < 200) {
                    	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                case "200-500":
                    if (entry.getValue().getPrice() >= 200 && entry.getValue().getPrice() < 500) {
                        dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                case "500-1000":
                    if (entry.getValue().getPrice() >= 500 && entry.getValue().getPrice() < 1000) {
                    	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                case ">1000":
                    if (entry.getValue().getPrice() >= 1000) {
                    	dtm4.addRow(new Object[] { entry.getValue().getId(), entry.getValue().getName(),
    							entry.getValue().getCategorie(),entry.getValue().getPrice(),entry.getValue().getStoc()});
                    }
                    break;
                default:
                }
            }
        }
		
		
	}


}

/**
 * Reseteaza tabelul adica sterge datele din tabelul pentru a efectua un alt search.
 */
public void reset(){
	dtm4.setRowCount(0);
}

/**
 * Gets the table.
 *
 * @return the table
 */
public JTable getTable() {
	return this.table;
}

/**
 * Gets the table2.
 *
 * @return the table2
 */
public JTable getTable2() {
	return this.table2;
}

/**
 * Gets the table3.
 *
 * @return the table3
 */
public JTable getTable3() {
	return this.table3;
}

/**
 * Gets the table4.
 *
 * @return the table4
 */
public JTable getTable4() {
	return this.table4;
}

}
