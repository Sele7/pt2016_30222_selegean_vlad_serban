package model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Model.
 */
public class Model implements Serializable {
	
	
	/** The warehouse. */
	private WareHouse warehouse = new WareHouse();
	
	/** The op dept. */
	private OPDept opDept = new OPDept();
	
	/**
	 * Instantiates a new model.
	 *
	 * @param warehouse the warehouse
	 * @param opDept the op dept
	 */
	public Model(WareHouse warehouse,OPDept opDept){
		this.warehouse=warehouse;
		this.opDept=opDept;
	}

	/**
	 * Gets the op dept.
	 *
	 * @return the op dept
	 */
	public OPDept getOpDept() {
		return opDept;
	}

	/**
	 * Gets the warehouse.
	 *
	 * @return the warehouse
	 */
	public WareHouse getWarehouse() {
		return warehouse;
	}
	
}
