package model;

// TODO: Auto-generated Javadoc
/**
 * The Class NotAvailableException - se refera la exceptia in cazul in care produsul nu exista in depozit.
 */
public class NotAvailableException extends Exception {

	/**
	 * Instantiates a new not available exception.
	 */
	public NotAvailableException() {
		super("! Nu exista in stoc.");
	}

	/**
	 * Instantiates a new not available exception.
	 *
	 * @param message the message
	 */
	public NotAvailableException(String message) {
		super(message);
	}
}
