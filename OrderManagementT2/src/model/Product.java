package model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Product.
 */
public class Product implements Serializable {

	/** The id. */
	private int id;
	
	/** The name. */
	private String name;
	
	/** The stoc. */
	private int stoc;
	
	/** The price. */
	private double price;
	
	/** The categorie. */
	private String categorie;

	/**
	 * Instantiates a new product.
	 *
	 * @param name the name
	 * @param id the id
	 * @param stoc the stoc
	 * @param price the price
	 * @param categorie the categorie
	 */
	public Product(String name, int id, int stoc, double price,String categorie) {
		super();
		this.name = name;
		this.id = id;
		this.stoc = stoc;
		this.price = price;
		this.categorie = categorie;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the stoc.
	 *
	 * @return the stoc
	 */
	public int getStoc() {
		return stoc;
	}

	/**
	 * Sets the stoc.
	 *
	 * @param stoc the new stoc
	 */
	public void setStoc(int stoc) {
		this.stoc = stoc;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Gets the categorie.
	 *
	 * @return the categorie
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * Sets the categorie.
	 *
	 * @param categorie the new categorie
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	
	

}
