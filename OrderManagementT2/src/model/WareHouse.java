package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

// TODO: Auto-generated Javadoc
/**
 * The Class WareHouse- Depozit , aici sunt depozitate produsele.
 */
public class WareHouse implements Serializable {

	/** The depozit. */
	private TreeMap<Integer, Product> depozit = new TreeMap<Integer, Product>();
	
	/** The space. */
	private int space = 250;
	
	/**
	 * Instantiates a new ware house.
	 */
	public WareHouse() {

	}

	/**
	 * Instantiates a new ware house.
	 *
	 * @param depozit the depozit
	 * @param space the space
	 */
	public WareHouse(TreeMap<Integer, Product> depozit, int space) {
		super();
		this.depozit = depozit;
		this.space = space;
	}

	/**
	 * Gets the depozit.
	 *
	 * @return the depozit
	 */
	public TreeMap<Integer, Product> getDepozit() {
		return depozit;
	}

	/**
	 * Sets the depozit.
	 *
	 * @param depozit the depozit
	 */
	public void setDepozit(TreeMap<Integer, Product> depozit) {
		this.depozit = depozit;
	}

	/**
	 * Gets the space.
	 *
	 * @return the space
	 */
	public int getSpace() {
		return space;
	}

	/**
	 * Sets the space.
	 *
	 * @param space the new space
	 */
	public void setSpace(int space) {
		this.space = space;
	}

	/**
	 * Metoda ne spunce cat spatiu ocupa produsele acum in depozit
	 *
	 * @return the current space
	 */
	public int getCurrentSpace() {
		int x = 0;
		for (Map.Entry<Integer, Product> entry : depozit.entrySet())
			x = x + entry.getValue().getStoc();

		return x;
	}

	/**
	 * returneaza pretul pentru produsul cautat
	 *
	 * @param name the name
	 * @return the product price
	 */
	public double getProductPrice(String name) {

		for (Map.Entry<Integer, Product> entry : depozit.entrySet())
			if (entry.getValue().getName().equals(name))
				return entry.getValue().getPrice();

		return -1;
	}

	/**
	 * Verifica daca produsul exista in depozit sau nu
	 *
	 * @param product the product
	 * @return true, if is available
	 */
	public boolean isAvailable(Product product) {

		if (depozit.get(product.getId()) != null)
			return true;
		else
			return false;

	}
	
	/**
	 * Modifica stocul cu cantitatea pe care o adaugam sau o scadem
	 * 
	 *
	 * @param productID the product id
	 * @param cantitate the cantitate
	 * @throws NotAvailableException the not available exception
	 */
	public void modifyStoc(int productID, int cantitate) throws NotAvailableException {

		if (!depozit.containsKey(productID)) {
			throw new NotAvailableException();
		} else {
			Product product = depozit.get(productID);
			depozit.put(productID, new Product(product.getName(), product.getId(), product.getStoc() + cantitate, product.getPrice(),product.getCategorie()));
		}
	}
	
	/**
	 * Adauga produse in depozit , in TreeMap trecand prin fiecare entry se verifica daca cumva exista produsul deja atunci metoda
	 * se comporta ca modifyStoc altfel se adauga in depozit daca mai este loc
	 *
	 * @param product the product
	 * @throws OverStockException the over stock exception
	 */
	public void addProduct(Product product) throws OverStockException {

		if (getCurrentSpace() + product.getStoc() > space) {
			throw new OverStockException();
		} else {
			if (depozit.containsKey(product.getId())) {
				try {
					modifyStoc(product.getId(),product.getStoc() + depozit.get(product.getId()).getStoc());
				} catch (NotAvailableException e) {
					e.printStackTrace();
				}
			} else {
				depozit.put(product.getId(), product);
			}
		}
	}

	/**
	 * Sterge un produs din depozit,TreeMap trecand prinf fiecare entry si cautand dupa cheia produsului , daca il gasim il eliminam
	 *
	 * @param id the id
	 * @throws NotAvailableException the not available exception
	 */
	public void removeProduct(int id) throws NotAvailableException {

		if (depozit.containsKey(id) == false) {
			throw new NotAvailableException();
		} else {
			for (Map.Entry<Integer, Product> entry : depozit.entrySet()) {
				if (entry.getKey() == id) {
					depozit.remove(entry.getKey());
				}
			}
		}
	}
	
	/**
	 * Modifica pretul unui produs din depozit,TreeMap
	 *
	 * @param id the id
	 * @param price the price
	 * @throws NotAvailableException the not available exception
	 */
	public void modifyProduct(int id,int price) throws NotAvailableException {

		if (depozit.containsKey(id) == false) {
			throw new NotAvailableException();
		} else {
			for (Map.Entry<Integer, Product> entry : depozit.entrySet()) {
				if (entry.getKey() == id) {
					depozit.get(id).setPrice(price);
				}
			}
			
		}
	}
	
	/**
	 * 
	 *
	 * @param id the id
	 * @return the produs
	 */
	public Product getProdus(int id)
	{
		return depozit.get(id);
	}
	
	/**
	 *	returneaza produsul pe care il cautam din treemap
	 *
	 * @param name the name
	 * @return the product2
	 */
	public Product getProduct2(String name) {

		for (Map.Entry<Integer, Product> entry : depozit.entrySet())
			if (entry.getValue().getName().equals(name))
				return entry.getValue();

		return null;
	}
	
	
	/**
	 * Adauga fiecare produs din treemap intro lista de produse care este folosita pentru a popila comboxul de produse
	 *
	 * @return the list
	 */
	public List<Product> productsList() {

		List<Product> produse = new ArrayList<Product>();
		for (Map.Entry<Integer, Product> entry : depozit.entrySet()) {
			produse.add(entry.getValue());
		}

		return produse;
	}
	
	/**
	 * Store warehouse. - Serializarea TreeMapului
	 */
	public void storeWarehouse() {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("depozit.ser"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            outputStream.writeObject(depozit);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
	/**
	 * Load warehouse. - Deserializarea TreeMapului
	 */
	public void loadWarehouse() {
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("depozit.ser");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream (fileIn);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Object obj;
        try {
            obj = inputStream.readObject();
            if (obj instanceof TreeMap)
            {
                this.setDepozit((TreeMap<Integer, Product>) obj);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
