package model;

// TODO: Auto-generated Javadoc
/**
 * The Class OverStockException - se refera la exceptia care apare daca incercam sa introducem mai multe produse decat stocul poate depozita.
 */
public class OverStockException extends Exception{

	/**
	 * Instantiates a new over stock exception.
	 */
	public OverStockException(){
		super("! Depasirea Cantitatii.");
	}
	
	/**
	 * Instantiates a new over stock exception.
	 *
	 * @param message the message
	 */
	public OverStockException(String message){
		super(message);
	}
}
