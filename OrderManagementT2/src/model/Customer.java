package model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Customer.
 */
public class Customer implements Serializable {

	/** The id. */
	private int id;
	
	/** The nume. */
	private String nume;
	
	/** The prenume. */
	private String prenume;
	
	/** The email. */
	private String email;

	/**
	 * Instantiates a new customer.
	 *
	 * @param id the id
	 * @param nume the nume
	 * @param prenume the prenume
	 * @param email the email
	 */
	public Customer(int id, String nume, String prenume, String email) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.email = email;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the nume.
	 *
	 * @return the nume
	 */
	public String getNume() {
		return nume;
	}

	/**
	 * Sets the nume.
	 *
	 * @param nume the new nume
	 */
	public void setNume(String nume) {
		this.nume = nume;
	}

	/**
	 * Gets the prenume.
	 *
	 * @return the prenume
	 */
	public String getPrenume() {
		return prenume;
	}

	/**
	 * Sets the prenume.
	 *
	 * @param prenume the new prenume
	 */
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Customer cu ID-ul " + id + ", Numele: " + nume + ", Prenumele: " + prenume + ", email: " + email + ".";
	}

}
