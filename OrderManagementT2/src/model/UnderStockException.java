package model;

// TODO: Auto-generated Javadoc
/**
 * The Class UnderStockException - apare cand se comanda un numar mai mare de produse decat cele disponibile in stoc/depozit.
 */
public class UnderStockException extends Exception {

	/**
	 * Instantiates a new under stock exception.
	 */
	public UnderStockException() {
		super("! Stoc insuficient.");
	}

	/**
	 * Instantiates a new under stock exception.
	 *
	 * @param message the message
	 */
	public UnderStockException(String message) {
		super(message);
	}

}
