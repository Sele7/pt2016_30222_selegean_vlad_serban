package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

// TODO: Auto-generated Javadoc
/**
 * The Class OPDept - Order process Dept.
 */
public class OPDept implements Serializable {
	
	/** The t comenzi. */
	TreeMap<Long,Order> tComenzi = new TreeMap<Long, Order>();
	
	/** The depozit. */
	private WareHouse depozit = new WareHouse();
	
	/** The clienti. */
	private List<Customer> clienti = new ArrayList<Customer>();
	
	/** The no. */
	private boolean no = false;
	
	/** The no2. */
	private boolean no2 = false;
	
	/**
	 * Instantiates a new OP dept.
	 */
	public OPDept(){
		
	}
	
	
	/**
	 * Instantiates a new OP dept.
	 *
	 * @param depozit the depozit
	 */
	public OPDept(WareHouse depozit){
		this.depozit = depozit;
	}
	
	/**
	 * Instantiates a new OP dept.
	 *
	 * @param tComenzi the t comenzi
	 * @param depozit the depozit
	 * @param clienti the clienti
	 */
	public OPDept(TreeMap<Long, Order> tComenzi, WareHouse depozit,List<Customer> clienti) {
		super();
		this.tComenzi = tComenzi;
		this.depozit = depozit;
		this.clienti = clienti;
	}
	
	/**
	 * Gets the no1. - verificam daca apare o exceptie cand dorim sa comandam gen Not Available 
	 *
	 * @return the no1
	 */
	public boolean getNo1(){
		return no;
	}
	
	/**
	 * Gets the no2 . - verificam daca apare o exceptie cand dorim sa comandam gen Under Stock 
	 *
	 * @return the no2
	 */
	public boolean getNo2(){
		return no2;
	}

	/**
	 * Gets the t comenzi.
	 *
	 * @return the t comenzi
	 */
	public TreeMap<Long, Order> gettComenzi() {
		return tComenzi;
	}

	/**
	 * Sett comenzi.
	 *
	 * @param tComenzi the t comenzi
	 */
	public void settComenzi(TreeMap<Long, Order> tComenzi) {
		this.tComenzi = tComenzi;
	}

	/**
	 * Gets the depozit.
	 *
	 * @return the depozit
	 */
	public WareHouse getDepozit() {
		return depozit;
	}

	/**
	 * Sets the depozit.
	 *
	 * @param depozit the new depozit
	 */
	public void setDepozit(WareHouse depozit) {
		this.depozit = depozit;
	}
	

	/**
	 * Gets the clienti.
	 *
	 * @return the clienti
	 */
	public List<Customer> getClienti() {
		return clienti;
	}

	/**
	 * Sets the clienti.
	 *
	 * @param clienti the new clienti
	 */
	public void setClienti(List<Customer> clienti) {
		this.clienti = clienti;
	}

	/**
	 * Plaseaza o comanda , mai exact trece prin treemap si verifica daca se poate face o comanda
	 * daca se poate se scade stocul
	 *
	 * @param order the order
	 * @throws NotAvailableException the not available exception
	 * @throws UnderStockException the under stock exception
	 */
	public void placeOrder(Order order) throws NotAvailableException,UnderStockException
	{
		if(depozit.isAvailable(order.getProdus()) == false)
			{no=true;throw new NotAvailableException();}
		else
			if(depozit.getProduct2(order.getProdus().getName()).getStoc() < order.getCantitate())
					{no2=true;throw new UnderStockException();}
		else
			depozit.modifyStoc(order.getProdus().getId(), -order.getCantitate());
	}
	
	/**
	 * sterge o comanda din treeMap in functie de id
	 *
	 * @param id the id
	 * @throws NotAvailableException the not available exception
	 */
	public void removeOrder(long id)  throws NotAvailableException
	{
//		if(tComenzi.get(order.getId())== null)
//			System.out.println("Comanda inexistenta");
//		else
//			tComenzi.remove(order.getId());
		
		if (tComenzi.containsKey(id) == false) {
			throw new NotAvailableException();
		} else {
			for (Map.Entry<Long,Order> entry : tComenzi.entrySet()) {
				if (entry.getKey() == id) {
					tComenzi.remove(entry.getKey());
				}
			}
		}
		
	}

	
	/**
	 * Removes the client.
	 *
	 * @param id the id
	 */
	public void removeClient(int id){
		for(int i = 0 ; i< clienti.size();i++)
		{
			if(clienti.get(i).getId()==id)
				clienti.remove(i);
		}
	}
	
	/**
	 * Edits the client.
	 *
	 * @param id the id
	 * @param nume the nume
	 * @param prenume the prenume
	 * @param email the email
	 */
	public void editClient(int id,String nume,String prenume,String email){
		for(int i = 0 ; i< clienti.size();i++)
		{
			if(clienti.get(i).getId()==id){
				clienti.get(i).setNume(nume);
				clienti.get(i).setPrenume(prenume);
				clienti.get(i).setEmail(email);}
		}
	}
	
	/**
	 * returneaza clientul cu id cautat
	 *
	 * @param id the id
	 * @return the client1
	 */
	public Customer getClient1(int id){
		for(int i = 0 ; i< clienti.size();i++)
		{
			if(clienti.get(i).getId()==id)
				return clienti.get(i);
		}
		return null;
	}
	
	/**
	 * Store customers. - Serializarea Clientilor
	 */
	public void storeCustomers() {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("customers.ser"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            outputStream.writeObject(clienti);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
	/**
	 * Load customers. - - Deserializarea Clientilor
	 */
	public void loadCustomers() {
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("customers.ser");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream (fileIn);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Object obj;
        try {
            obj = inputStream.readObject();
            if (obj instanceof List)
            {
                this.setClienti((List<Customer>) obj);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	/**
	 * Store orders.-  Serializarea Comenzilor
	 */
	public void storeOrders() {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("orders.ser"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            outputStream.writeObject(tComenzi);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
	/**
	 * Load orders. Deserializarea Comenzilor
	 */
	public void loadOrders() {
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("orders.ser");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream (fileIn);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Object obj;
        try {
            obj = inputStream.readObject();
            if (obj instanceof TreeMap)
            {
                this.settComenzi((TreeMap<Long,Order>) obj);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
}
