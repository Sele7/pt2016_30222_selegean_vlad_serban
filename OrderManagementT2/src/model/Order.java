package model;

import java.io.Serializable;
import java.sql.Timestamp;

// TODO: Auto-generated Javadoc
/**
 * The Class Order.
 */
public class Order implements Serializable {

	/** The id. */
	private int id;
	
	/** The customer. */
	private Customer customer;
	
	/** The produs. */
	private Product produs;
	
	/** The cantitate. */
	private int cantitate;
	
	/** The time stamp. */
	private Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
	
	/** The total. */
	private double total;
	
	/**
	 * Instantiates a new order.
	 *
	 * @param id the id
	 * @param customer the customer
	 * @param produs the produs
	 * @param cantitate the cantitate
	 * @param timeStamp the time stamp
	 * @param total the total
	 */
	public Order(int id, Customer customer, Product produs, int cantitate, Timestamp timeStamp,double total) {
		super();
		this.id = id;
		this.customer = customer;
		this.produs = produs;
		this.cantitate = cantitate;
		this.timeStamp = timeStamp;
		this.total = total;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer the new customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * Gets the produs.
	 *
	 * @return the produs
	 */
	public Product getProdus() {
		return produs;
	}

	/**
	 * Sets the produs.
	 *
	 * @param produs the new produs
	 */
	public void setProdus(Product produs) {
		this.produs = produs;
	}

	/**
	 * Gets the cantitate.
	 *
	 * @return the cantitate
	 */
	public int getCantitate() {
		return cantitate;
	}

	/**
	 * Sets the cantitate.
	 *
	 * @param cantitate the new cantitate
	 */
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param timeStamp the new time stamp
	 */
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the new total
	 */
	public void setTotal(int total) {
		this.total = total;
	}
	
	
	

}
