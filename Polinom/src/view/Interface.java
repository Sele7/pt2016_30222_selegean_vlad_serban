package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import model.Monom;
import model.Polynom;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Aceasta este clasa numita Interfata.
 * Reprezinta View-ul proiectului
 * Interfata este facuta in Swing , creata cu Window Builder
 */
public class Interface {

	/** Frame-ul. */
	public JFrame frame;
	
	/** Primul TextField ce va fi locul in care se introduce primul polinom */
	private JTextField textField;
	
	/** Al Doilea TextField ce va fi locul in care se introduce al doilea polinom */
	private JTextField textField_1;
	
	/** Al Treilea TextField ce va fi locul in care se introduce rezultatul dat de operatii asupra polinoamelor */
	private JTextField textField_2;
	
	/** Reprezinta butonul de Adunare */
	JButton btnNewButton = new JButton("Adunare");
	
	/** Reprezinta butonul de Scadere */
	JButton btnScadere = new JButton("Scadere");
	
	/** Reprezinta butonul de Inmultire */
	JButton btnInmultire = new JButton("Inmultire");
	
	/** Reprezinta butonul de Impartire */
	JButton btnImpartire = new JButton("Impartire");
	
	/** Reprezinta butonul de Derivare */
	JButton btnDerivare = new JButton("Derivare");
	
	/** Reprezinta butonul de Integrare */
	JButton btnIntegrare = new JButton("Integrare");

	/**
	 * Lansarea aplicatiei swing , creata de Window Builder
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * In constructorul Interfetei se seteaza componentele si se adauga pane-ului respectiv frame-ului
	 */
	public Interface() {
		//initialize();
	

	/**
	 * Se seteaza componentele frame-ului
	 * Titlu,Terminarea procesului cand apasam X,Layout-ul frame-ului.
	 */
	//private void initialize() {
		frame = new JFrame("Polynom Calculator");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	/** Setarile primului TextField si adaugarea la Pane*/	
		textField = new JTextField();
		textField.setBounds(99, 32, 299, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		/** Setarile pentru al 2 -lea TextField si adaugarea la Pane*/	
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(99, 63, 299, 20);
		frame.getContentPane().add(textField_1);
		
		/** Setarea primului label de langa primul textField si adica Polinom1.Adaugarea la pane*/
		JLabel lblNewLabel = new JLabel("Polinom1");
		lblNewLabel.setFont(new Font("Impact", Font.PLAIN, 11));
		lblNewLabel.setBounds(22, 35, 67, 14);
		frame.getContentPane().add(lblNewLabel);
		
		/** Setarea pentru al 2 -lea label de langa al 2 -lea textField si adica Polinom2.Adaugarea la pane*/
		JLabel lblPolinom = new JLabel("Polinom2");
		lblPolinom.setFont(new Font("Impact", Font.PLAIN, 11));
		lblPolinom.setBounds(22, 66, 67, 14);
		frame.getContentPane().add(lblPolinom);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Adunare*/
		//JButton btnNewButton = new JButton("Adunare");
		btnNewButton.setBounds(22, 115, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Scadere*/
		//JButton btnScadere = new JButton("Scadere");
		btnScadere.setBounds(121, 115, 89, 23);
		frame.getContentPane().add(btnScadere);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Inmultire*/
		//JButton btnInmultire = new JButton("Inmultire");
		btnInmultire.setBounds(220, 115, 89, 23);
		frame.getContentPane().add(btnInmultire);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Impartire*/
		//JButton btnImpartire = new JButton("Impartire");
		btnImpartire.setBounds(319, 115, 89, 23);
		frame.getContentPane().add(btnImpartire);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Derivare*/
		//JButton btnDerivare = new JButton("Derivare");
		btnDerivare.setBounds(99, 159, 89, 23);
		frame.getContentPane().add(btnDerivare);
		
		/** Pozitionarea si adaugarea la Pane a butonului de Integrare*/
		//JButton btnIntegrare = new JButton("Integrare");
		btnIntegrare.setBounds(255, 159, 89, 23);
		frame.getContentPane().add(btnIntegrare);
		
		/** Setarile pentru al 3 -lea TextField(rezultatul) si adaugarea la Pane*/	
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(99, 208, 299, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		/** Setarea pentru al 3 -lea label de langa al 3 -lea  textField si adica Rezultat.Adaugarea la pane*/
		JLabel lblNewLabel_1 = new JLabel("Rezultat");
		lblNewLabel_1.setFont(new Font("Impact", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(22, 211, 67, 14);
		frame.getContentPane().add(lblNewLabel_1);}
	
	
	/**
	 * In aceasta functie se i-au datele din primul TextField -> Polinom1
	 * Functioneaza ca si un getter
	 * @return the string
	 */
	public String primulPolinom(){        
		return (textField.getText());}
	
	/**
	 * In aceasta functie se i-au datele din al 2-lea TextField -> Polinom2
	 * Functioneaza ca si un getter
	 * @return the string
	 */
	public String alDoileaPolinom(){        
		return (textField_1.getText());}
	
	 /**
 	 * In aceasta functie se seteaza ce rezultat sa afisam in TextFieldul Rezultat
 	 * Functioneaza ca si un setter
 	 * @param s3 the new calc solution
 	 */
 	public void setCalcSolution(String s3){
		 textField_2.setText(s3.toString());}
	 
	 /**
 	 * Creeam Ascultatorii(Listener) pentru butoane , acesta pentru butonul de adunare.
 	 * Folositi pentru a sti cand apasam pe buton.
 	 * @param listenForAdunare the listen for adunare
 	 */
 	public void adunareListener(ActionListener listenForAdunare){
		 btnNewButton.addActionListener(listenForAdunare);
		}
	 
 	/**
 	 * Listener-ul pentru butonul de scadere
 	 * @param listenForScadere the listen for scadere
 	 */
 	public void scadereListener(ActionListener listenForScadere){
		 btnScadere .addActionListener(listenForScadere);
		}
	 
 	/**
 	 * Listener-ul pentru butonul de inmultire
 	 * @param listenForInmultire the listen for inmultire
 	 */
 	public void inmultireListener(ActionListener listenForInmultire){
		 btnInmultire .addActionListener(listenForInmultire);
		}
	 
 	/**
 	 * Listener-ul pentru butonul de impartire
 	 * @param listenForImpartire the listen for impartire
 	 */
 	public void impartireListener(ActionListener listenForImpartire){
		 btnImpartire .addActionListener(listenForImpartire);
		}
	 
 	/**
 	 * Listener-ul pentru butonul de derivare
 	 * @param listenForDerivare the listen for derivare
 	 */
 	public void derivareListener(ActionListener listenForDerivare){
		 btnDerivare .addActionListener(listenForDerivare);
		}
	 
 	/**
 	 * Listener-ul pentru butonul de integrare
 	 * @param listenForIntegrare the listen for integrare
 	 */
 	public void integrareListener(ActionListener listenForIntegrare){
		 btnIntegrare .addActionListener(listenForIntegrare);
		}
	 
	 /**
 	 * Functie ce va afisa un mesaj de eroare in funtie de ce errorMessage v-om da
 	 * De exemplu o sa avem in Control un errorMsg : Atentie la Introducerea Polinoamelor..
 	 * @param errorMessage the error message
 	 */
 	public void displayErrorMessage(String errorMessage){
         JOptionPane.showMessageDialog(frame, errorMessage);}

	 
		
}
