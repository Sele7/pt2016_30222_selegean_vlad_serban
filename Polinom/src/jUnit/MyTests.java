package jUnit;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import model.Calculator;
import model.Monom;
import model.Polynom;
/**Clasa folosita pentru testarea JUnit*/
public class MyTests {
	private Calculator calc = new Calculator();
	
	/** Aceasta metoda calculeaza diferenta intre 2 pol. si daca acele 2 polinoame sunt acceleasi , adica
	 * diferenta va da 0 , aceasta returneaza true*/
	public boolean esteZero(Polynom a,Polynom b){
		Polynom c = new Polynom();
		c = calc.scadere(a, b);
		boolean OK = true;
		System.out.println(c.getMonoms().size());
		for(int i=0;i<c.getMonoms().size();i++)
		{

			System.out.println(c.getMonoms().get(i).getCoeficient().intValue());
			if(c.getMonoms().get(i).getCoeficient().intValue() == 0)
				OK=true;
			else 
				OK=false;
		}
		
			return OK;
		
	}
	
	public boolean verificaAdunare(Polynom a,Polynom b){
		Polynom c = new Polynom();
		c = calc.adunare(a, b);
		int coefOK=0;
		boolean OK = true;
		
		for(int i=0;i<c.getMonoms().size();i++){
			if(c.getMonoms().get(i).getCoeficient().intValue() == a.getMonoms().get(i).getCoeficient().intValue() + b.getMonoms().get(i).getCoeficient().intValue())
				coefOK=1;
			else
				OK=false;
			if((c.getMonoms().get(i).getPutere() == a.getMonoms().get(i).getPutere() || c.getMonoms().get(i).getPutere() == b.getMonoms().get(i).getPutere()) && coefOK == 1)
				OK = true;
			else
				OK = false;}
		return OK;
	}
	
	public boolean verificaScadere(Polynom a,Polynom b){
		Polynom c = new Polynom();
		c = calc.scadere(a, b);
		int coefOK=0;
		boolean OK = true;
		
		for(int i=0;i<c.getMonoms().size();i++){
			if(c.getMonoms().get(i).getCoeficient().intValue() == a.getMonoms().get(i).getCoeficient().intValue() - b.getMonoms().get(i).getCoeficient().intValue())
				coefOK=1;
			else
				OK=false;
			if((c.getMonoms().get(i).getPutere() == a.getMonoms().get(i).getPutere() || c.getMonoms().get(i).getPutere() == b.getMonoms().get(i).getPutere()) && coefOK == 1)
				OK=true;
			else
				OK = false;
			}
		return OK;
	}
	/**Metoda unde testam operatiile*/
	@Test
	public void testAdunare(){
		
		//operatiile din clasa Calculator sunt testate
		
		
		//asserturile:
		
		Polynom a = new Polynom();
		Polynom b = new Polynom();
		Polynom c = new Polynom();
		Polynom d = new Polynom();
		List<Monom> l = new ArrayList<Monom>();
		
		a.getMonoms().add(new Monom(1,0));
		a.getMonoms().add(new Monom(1,1));
		a.getMonoms().add(new Monom(2,2));
		
		b.getMonoms().add(new Monom(1,0));
		b.getMonoms().add(new Monom(1,1));
		b.getMonoms().add(new Monom(1,2));
		//b.monoms.add(new Monom(1,1));
		
		l.add(new Monom(2,0));
		l.add(new Monom(2,1));
		l.add(new Monom(2,2));
		
		c=calc.scadere(a, b);
		System.out.println(c.getMonoms());
		
		d.getMonoms().add(new Monom(2,0));
		d.getMonoms().add(new Monom(2,1));
		d.getMonoms().add(new Monom(2,2));
		
		
		//assertTrue(esteZero(a, b));
		assertTrue(verificaAdunare(a, b));
		assertTrue(verificaScadere(a, b));
//		assertEquals(l, calc.adunare(a, b).getMonoms());
//		System.out.println(calc.adunare(a, b).getMonoms());
//		System.out.println(l);
	}

}
