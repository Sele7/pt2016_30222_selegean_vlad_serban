package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import model.Calculator;
import model.Monom;
import model.Polynom;
import view.Interface;

// TODO: Auto-generated Javadoc
/**
 * Clasa Control mai exact clasa Controller din pattern-ul MVC.
 */
public class Control {
	
	/** View-ul din pattern-ul MVC */
	private Interface View;
	
	/** Model-ul din pattern-ul MVC  */
	private Calculator Model;
	
	/**
	 * In constructor incarcam datele petru View si Model.
	 * Se adauga listenerii pentru fiecare buton 
	 * @param view the view
	 * @param model the model
	 */
	public Control(Interface view, Calculator model) {
		View = view;
		Model = model;
		
		this.View.adunareListener(new AdunareListener());
		this.View.scadereListener(new ScadereListener());
		this.View.inmultireListener(new InmultireListener());
		this.View.impartireListener(new ImpartireListener());
		this.View.derivareListener(new DerivareListener());
		this.View.integrareListener(new IntegrareListener());

	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul adunare.
	 * In aceasta clasa se face adunarea a 2 polinoame , dupa ce acestea au fost introduse si butonul de Adunare este apasat
	 * @see AdunareEvent
	 */
	private class AdunareListener implements ActionListener{
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			/** Initializam 3 polinoame 2 pentru primele 2 polinoame iar c pentru polinomul rezultat*/
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();

			/** Initializma 3 stringuri pentru extragerea si introducerea datelor din/in textfielduri*/
			String s1;
			String s2;
			String s3;
			
			try{
				/** Luam datele din textFielduri , textF1 si TextF1*/
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				/** Parsam primul string si adaugam datele in polinomul a*/
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				/** Parsam al 2-lea string si adaugam datele in polinomul b*/
				getMonoame(s2,b);
				System.out.println(b.getMonoms());
				//a = View.primulPolinom();
				//b = View.alDoileaPolinom();
				/** Are loc adunarea propriu zisa ce returneaza un polinom , il salvam in c .
				 * In s3 salvam datele din C in forma string , folosim .toString , pentru a putea fi afisate in TextFieldul Rezultat*/
				c=Model.adunare(a, b);
				s3 = c.toString();
				System.out.println(s3);
				View.setCalcSolution(s3);
			
			}
			/** Arunca o exceptie daca polinoamele nu sunt introduse corect*/
			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul scadere.
	 * In aceasta clasa se face scaderea a 2 polinoame , dupa ce acestea au fost introduse si butonul de Scadere este apasat
	 * Acceasi gandire ca la Adunare atat ca se face scaderea.
	 * @see ScadereEvent
	 */
	private class ScadereListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			
			String s1;
			String s2;
			String s3;
			try{
				
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				getMonoame(s2,b);
				System.out.println(b.getMonoms());
				//a = View.primulPolinom();
				//b = View.alDoileaPolinom();
				
				c=Model.scadere(a, b);
				s3 = c.toString();
				System.out.println(s3);
				View.setCalcSolution(s3);
				//View.setCalcSolution(c);
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul inmultire.
	 * In aceasta clasa se face inmultirea a 2 polinoame , dupa ce acestea au fost introduse si butonul de inmultire este apasat
	 * Acceasi gandire ca la Adunare atat ca se face inmultirea.
	 *
	 * @see InmultireEvent
	 */
	private class InmultireListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			
			String s1;
			String s2;
			String s3;
			try{
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				getMonoame(s2,b);
				System.out.println(b.getMonoms());
				
				
				//a = View.primulPolinom();
				//b = View.alDoileaPolinom();
				
				c=Model.inmultire(a, b);
				
				s3 = c.toString();
				System.out.println(s3);
				View.setCalcSolution(s3);
				//View.setCalcSolution(c);
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul Impartire.
	 * In aceasta clasa se face Impartirea a 2 polinoame , dupa ce acestea au fost introduse si butonul de Impartire este apasat
	 * Acceasi gandire ca la Adunare atat ca se face Impartire.
	 * Avem in plus faptul ca trebuie sa returnam atat catul cat si restul , astfel folosim o lista de polinoame
	 * unde pe pozitia 0 salvam catul iar pe pozitia 1 restul, de aceea avem un string , s4, in plus pentru rest.
	 * 
	 * @see ImpartireEvent
	 */
	private class ImpartireListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			
			List<Polynom> r = new ArrayList<Polynom>();
			
			String s1;
			String s2;
			String s3;
			String s4;
			try{
				
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				getMonoame(s2,b);
				System.out.println(b.getMonoms());
				//a = View.primulPolinom();
				//b = View.alDoileaPolinom();
				
				r=Model.impartire(a, b);//returneaza o lista de polinoame
				
				s3 = r.get(0).toString();//cat
				s4 = r.get(1).toString();//rest
				System.out.println(s3);
				View.setCalcSolution("Cat: " + s3 + " Rest: " + s4);
				//View.setCalcSolution(c);
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul derivare.
	 * In aceasta clasa se face derivarea a unui polinom , dupa ce acesta a fost introdus si butonul de derivare este apasat
	 * Acceasi gandire ca la Adunare atat ca se face derivarea si deci se foloseste cu un polinom mai putin.
	 * method is invoked.
	 *
	 * @see DerivareEvent
	 */
	private class DerivareListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			
			String s1;
			String s2;
			
			try{
				
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				//a = View.primulPolinom();
				
				b=Model.derivare(a);
				
				s2 = b.toString();
				System.out.println(s2);
				View.setCalcSolution(s2);
				
				//View.setCalcSolution(b);
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	/**
	 * Clasa ce primeste eventurile legate de butonul Integrare.
	 * In aceasta clasa se face Integrarea a unui polinom , dupa ce acesta a fost introdus si butonul de Integrare este apasat
	 * Acceasi gandire ca la Adunare atat ca se face Integrarea si deci se foloseste cu un polinom mai putin.
	 *
	 * @see IntegrareEvent
	 */
	private class IntegrareListener implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			
			String s1;
			String s2;
			try{
				
				s1 = View.primulPolinom();
				s2 = View.alDoileaPolinom();
				getMonoame(s1,a);
				System.out.println(a.getMonoms());
				//a = View.primulPolinom();
				
				b=Model.integrare(a);
				
				s2 = b.toString();
				System.out.println(s2);
				View.setCalcSolution(s2);
				
				//View.setCalcSolution(b);
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				View.displayErrorMessage("Atentie la Introducerea Polinoamelor");
				
			}
		}
		
	}
	
	 /**
 	 * Aceasta functie face parsarea datelor dintrun string adica dintr-un string ce exemplifica un polinom aceste date
 	 * sunt luate si adaugate in polinomul p.
 	 *
 	 * Cum functioneaza se citeste stringul se citeste pana se gaseste un 'x' in stanga se merge pana la primul semn
 	 * + sau - si se ia acest coeficient si se adauga in concordanta cu gradul la care se afla. Astfel tot de la acel 'x'
 	 * se merge in dreapta pana la primul plus si se ia gradul si se adauga monomului.
 	 * 
 	 * @param polynom the polynom
 	 * @param p the p
 	 * @return the monoame
 	 */
 	public void getMonoame(String polynom,Polynom p) {
	        String substring;
	        String degree;
	        String coef = null;
	        int i, j;
	        for (j = 0; j < polynom.length(); j++)
	        	if (polynom.charAt(j) == 'x'){
	        		for (i = j + 1; i < polynom.length(); i++)//citim cu i in fata dam de un +/- si luam gradul de la 
	        			if (polynom.charAt(i) == '+' || polynom.charAt(i) == '-') break;//j+2 care este inceputul gradului
	        		 degree = polynom.substring(j + 2, i);//pana la semnul de +/- de dupa el.
	        		 
	        		for (i = j; i > 0; i--) {//aici mergem sa luam coeficientul la fel pana la primul semn
	        			 if (polynom.charAt(i) == '+' || polynom.charAt(i) == '-') break;//atat ca mergem cu --
	        		coef = polynom.substring(i-1, j);//ajungem la +/- atunci luam de la semn ,cu tot cu el, pana la x
	        		}
	    
	        		Monom monom = new Monom(Integer.parseInt(coef),Integer.parseInt(degree));

	        		p.getMonoms().add(monom);
	        	}
	}

	
	

}
