package model;

import java.util.ArrayList;
import java.util.List;
/**Clasa Polynom care contine o lista de monoame astfel formand un polinom*/
public class Polynom {
	protected List<Monom> monoms = new ArrayList<Monom>();
	
	/** Aceasta metoda returneaza gradul in functie de dimensiunea listei - 1 
	 *  Aceasta are cateva limitari dar le-am rezolvat in metodele care apare cum ar fi in adunare,scadere,etc*/
	public int grad(){
		if(monoms.size() > 1)
			return monoms.size() - 1;
		else
			return monoms.size();
	}
	/**Getterul polinomului mai exact al listei  */
	public List<Monom> getMonoms() {
		return monoms;
	}
	/**Setterul polinomului mai exact al listei */
	public void setMonoms(List<Monom> monoms) {
		this.monoms = monoms;
	}
	
	/*
	public void instantiere(){
		for(int i=0;i<monoms.size();i++)
			monoms.add(new Monom(0,0));
	}*/
	/** Acest toString practic foloseste .toString-ul de la monoame si le concateneaza*/
	public String toString()
	{
		String s="";
		s=monoms.get(0).toString();
		for(int i=1;i<monoms.size();i++)
		{
			
				s=s+ " + " +monoms.get(i).toString();
		}
		return s;
	}
	
	public boolean unitate ()//returneaza true daca polinomul este 1x^0 si adica 1
	{
		for(int i = 0 ; i< monoms.size();i++)
		{
			if(monoms.get(0).coeficient.intValue() == 1 && monoms.get(i).coeficient.intValue() == 0)
				return true;
		}
		
		return false;
		
	}
	
	

}
