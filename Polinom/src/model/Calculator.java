package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Clasa Calculator care prezinta metodele de calculare a operatiilor adunare,scadere,etc.
 */
public class Calculator implements Metode{
	
	

	/* (non-Javadoc)
	 * @see model.Metode#adunare(model.Polynom, model.Polynom)
	 */
	@Override
	/** Metoda de adunare desigur avem 3 polinoame , pol. c este rezultatul
	 *  In size vedem care dintre polinoame are un grad mai mare pentru a sti cate monoame sa creez in c
	 *  */
	public Polynom adunare(Polynom a, Polynom b) {
		// TODO Auto-generated method stub 
		Polynom c = new Polynom();
		int dif=0;
		Integer suma=0;
		int size=Math.max(a.grad(), b.grad());
		
		for(int i=0;i<size;i++)//aici am cautat sa vad care este mai mare si in funtie de asta am mai adaugat cate
			if(a.grad()-b.grad() < 0)//monoame in functie de cate este nevoie pentru a putea face adunarea
				{
					dif=Math.abs(a.grad()-b.grad());
					for(int j=0;j<dif;j++)
						a.monoms.add(new Monom(0,0));
				}
			else
				{
					dif=Math.abs(a.grad()-b.grad());
					for(int j=0;j<dif;j++)
						b.monoms.add(new Monom(0,0));
				}
				
		
		size++;//crestem size-ul deoare functia .grad() -> list.size() - 1 astfel daca nu facem ++ -> IndexOutOfBounce
		for(int i=0;i<size;i++)
			c.monoms.add(new Monom(0,0));
		
		for(int i=0;i<size;i++)//adunam element cu element si setam coeficientii respectiv puterile
		{
			suma = a.monoms.get(i).getCoeficient().intValue() + b.monoms.get(i).getCoeficient().intValue();
			c.monoms.get(i).setCoeficient(suma);
			c.monoms.get(i).setPutere(i);

		}

		return c;
	}

	/* (non-Javadoc)
	 * @see model.Metode#scadere(model.Polynom, model.Polynom)
	 */
	@Override
	/** Metoda de scadere desigur avem 3 polinoame , pol. c este rezultatul
	 *  In size vedem care dintre polinoame are un grad mai mare pentru a sti cate monoame sa creez in c
	 *  */
	public Polynom scadere(Polynom a, Polynom b) {
		// TODO Auto-generated method stub
		Polynom c = new Polynom();
		int dif=0;
		Integer diferenta=0;
		int size=Math.max(a.grad(), b.grad());
		
		for(int i=0;i<size;i++)//acc. lucru ca si la adunare
			if(a.grad()-b.grad() < 0)
				{
					dif=Math.abs(a.grad()-b.grad());
					for(int j=0;j<dif;j++)
						a.monoms.add(new Monom(0,0));
				}
			else
				{
					dif=Math.abs(a.grad()-b.grad());
					for(int j=0;j<dif;j++)
						b.monoms.add(new Monom(0,0));
				}
				
		
		size++;
		for(int i=0;i<size;i++)
			c.monoms.add(new Monom(0,0));
		
		for(int i=0;i<size;i++)//facem scaderea element cu element
		{
			diferenta = a.monoms.get(i).getCoeficient().intValue() - b.monoms.get(i).getCoeficient().intValue();
			c.monoms.get(i).setCoeficient(diferenta);
			c.monoms.get(i).setPutere(i);

		}

		return c;
	}

	/* (non-Javadoc)
	 * @see model.Metode#inmultire(model.Polynom, model.Polynom)
	 */
	@Override
	/** Metoda de inmultire desigur avem 3 polinoame , pol. c este rezultatul
	 *  Adaugam atatea monoame in C cate avem in size adica suma gradelor
	 *  */
	public Polynom inmultire(Polynom a, Polynom b) {
		// TODO Auto-generated method stub
		Polynom c = new Polynom();
		int size=a.grad() + b.grad();
		Integer inmultire=0;
		Integer add=0;
		Integer add2=0;
		int a1=a.grad()+1;// <=> lista.size()
		int b1=b.grad()+1;
		size=size+1;
		for(int i=0;i<size;i++)
			c.monoms.add(new Monom(0,0));
		
		for(int i=0;i<a1;i++)//inmultimim element cu element si adaugam coeficientii de acc putere 
			for(int j=0;j<b1;j++)
			{
				if (b.monoms.get(0).coeficient.intValue() == 1 && b.monoms.size() == 1)
					return a;//daca inmultim cu 1 sa returneaze direct pol. a
				else
				{	
				inmultire = a.monoms.get(i).getCoeficient().intValue() * b.monoms.get(j).getCoeficient().intValue();
				add=(Integer) c.monoms.get(i+j).getCoeficient();
				add2=add+c.monoms.get(i+j).coef(inmultire);
				c.monoms.get(i+j).setCoeficient(add2);}
				
				
			}
		
		for(int i=0;i<size;i++)
		{
			c.monoms.get(i).setPutere(i);
			//c.monoms.get(i).setCoeficient(add2);
		}
			
				
		return c;
	}

	/* (non-Javadoc)
	 * @see model.Metode#impartire(model.Polynom, model.Polynom)
	 */
	@Override
	/** Metoda de impartire avem mai multe polinoame 2 auxiliare un cat , rest si cele 2 polinoame a si  b + lista
	 *  de polinoame unde salval restul si catul
	 *  Adaugam atatea monoame in C cate avem in size adica gradul cel mai mic
	 *  Algoritmul funct ca si impartirea normala , umpic mai ciudat astfel impartirea nu functioneaza mereu
	 *  */
	public List<Polynom> impartire(Polynom a, Polynom b) {
		// TODO Auto-generated method stub
		Polynom rem = new Polynom();
		Polynom aux = new Polynom();
		Polynom div = new Polynom();
		Polynom aux2 = new Polynom();
		
		List<Polynom> rezultate = new ArrayList<Polynom>();
		
		int size = a.grad();
		if (size == 1)
		{
			for(int i=0;i<=size;i++)//adagaum monoamele necesare
			{
				div.monoms.add(new Monom(0,0));
				aux.monoms.add(new Monom(0,0));
				rem.monoms.add(new Monom(0,0));
				aux2.monoms.add(new Monom(0,0));
			}
		}
		else
		{
			for(int i=0;i<size;i++)
			{
				div.monoms.add(new Monom(0,0));
				aux.monoms.add(new Monom(0,0));
				rem.monoms.add(new Monom(0,0));
				aux2.monoms.add(new Monom(0,0));
			}
		}
		
			
		int grB = b.grad();
		int grA = a.grad();
		int prim=0;
		int secund = 0;
		int i=0;
		if(grA == grB)//daca avem acc grad are loc o singura impartire 
			i=0;
		else
			i=b.grad();//mai multe impartiri pt grade diferite
		
		int grA2 =0;
		
		if ((b.grad() == 0) && (b.monoms.get(0).getCoeficient().intValue() == 0))//eroare pt impartire la 0
	        throw new RuntimeException("Impartire la 0!");
		if ((b.monoms.get(0).getPutere() == 0) && (b.monoms.get(0).getCoeficient().intValue() == 1) && b.monoms.size()==1)
		{
			rezultate.add(a);
			rezultate.add(rem);
			return rezultate;
		}
		else
		{
		
		//while(b.grad() >= rem.grad() && b.monoms.get(grB).getCoeficient().intValue() >= rem.monoms.get(grB).getCoeficient().intValue())
		do{
			grA = a.grad();
			prim = a.grad() - b.grad();//diferenta ne ofera gradul maxim dupa impartire
			//System.out.println("");
			//System.out.println(prim);
			div.monoms.get(i).setPutere(prim);
			secund = a.monoms.get(grA).getCoeficient().intValue()/b.monoms.get(grB).getCoeficient().intValue();
			div.monoms.get(i).setCoeficient(secund);
			
			System.out.println("Div " + div.monoms + div.grad());
			System.out.println(div.unitate());
			if(div.unitate() == true)
				aux=b;
			else
			{aux = inmultire(b, div);
			if(aux.grad() >= grA+1)
				aux.monoms.remove(grA2);
			System.out.println("Aux " + aux.monoms);
			aux2 = scadere(a, aux);
			//System.out.println(a);
			grA2 = a.grad();
			System.out.println("Aux2 " + aux2);		
			if(grA2 > grA)
				a.monoms.remove(grA2);//dupa scadere se poate ca sa adaugam un monom in plus ca sa putem face impar.
			a.monoms.remove(grA);//dupa o impartire reducem cu un grad ca sa putem verifica mai departe
			i--;//astfel trb sa scoatem acel monom ca sa nu modificam gradul lui a
			rem = aux2;
			System.out.println("Rem " + rem);
			//rem.monoms.remove(grA);//aux2 are cu un grad mai mult decat restu astfel scoatem un monom 
			prim--;}
			
			/*if(i == -1)
			{
				rezultate.add(div);
				rezultate.add(rem);
				System.out.println("Restul este" + rem.monoms);
				System.out.println("Catul este: " + div.monoms);
				return rezultate;
				
			}*/
				


		}while(b.grad() <= rem.grad() && b.monoms.get(grB).getCoeficient().intValue() <= rem.monoms.get(grB).getCoeficient().intValue());
	}
		rezultate.add(div);
		rezultate.add(rem);
		System.out.println("Restul este" + rem.monoms);
		System.out.println("Catul este: " + div.monoms);
		return rezultate;
	}

	/* (non-Javadoc)
	 * @see model.Metode#derivare(model.Polynom)
	 */
	@Override
	/**In metoda de derivare avem 2 polinoame c fiind rezultatul
	 * Cum funtioneaza: adaug monoamele necesare in c si pt fiecare putere o inmultim cu coeficientul si scadem cu 1 puterea*/
	public Polynom derivare(Polynom a) {
		// TODO Auto-generated method stub
		Polynom c = new Polynom();
		int size = a.monoms.size();
		Integer putere=0;
		Integer inm=0;
		
		for(int i=0;i<size;i++)
			c.monoms.add(new Monom(0,0));
		
		for(int i=0;i<size;i++)
		{
			putere = a.monoms.get(i).getPutere();
			if (putere > 0)
			{
				inm = (Integer)a.monoms.get(i).getCoeficient() * putere;
				c.monoms.get(i).setCoeficient(inm);
				putere--;
				c.monoms.get(i).setPutere(putere);
			}
			else
			{
				c.monoms.get(i).setCoeficient(0);//pentru termen liber 1x^0 de exemplu dupa derivare este 0
				c.monoms.get(i).setPutere(0);
			}
			
			
			
		}
			
		return c;
	}

	/* (non-Javadoc)
	 * @see model.Metode#integrare(model.Polynom)
	 */
	@Override
	/** Pentru integrare avem tot 2 polinoame , c fiind rezultatul
	 *  Cum functioneaza: Dupa ce initializma C-ul
	 *  crestem gradul fiecarui element si fiecare coeficient va fi impartit la acesta*/
	public Polynom integrare(Polynom a) {
		// TODO Auto-generated method stub.
		Polynom c = new Polynom();
		int size = a.monoms.size();
		Integer putere=0;
		Double imp=0.0;
		int coef=0;
		double coef2=0.0;
		
		for(int i=0;i<size;i++)
			c.monoms.add(new Monom(0,0));
		
		for(int i=0;i<size;i++)
		{
			putere = a.monoms.get(i).getPutere();
			if (putere > 0)
			{
				putere++;
				coef = a.monoms.get(i).getCoeficient().intValue();
				//coef2 = (double)coef;
				imp = (double)coef/putere;
				c.monoms.get(i).setCoeficient(imp);
				c.monoms.get(i).setPutere(putere);
			}
			else
			{
				c.monoms.get(i).setCoeficient(a.monoms.get(0).getCoeficient());//daca este termen liber pastram coef
				c.monoms.get(i).setPutere(1);//si facem un x^1
			}
		}
		return c;
	}

}
