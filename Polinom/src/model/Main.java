package model;

import controller.Control;
import view.Interface;
/** Clasa Main in care se instantiaza asa zisul MVC*/
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 /**Instantiam un View->Interface , un Model->Calculator si un Controller->Control
		  * si facem ca fereastra creata in view sa fie vizibila*/
		 Interface View = new Interface();
		 Calculator Model = new Calculator();
		 Control Controller = new Control(View,Model);
		 View.frame.setVisible(true);
		
		
	}

}
