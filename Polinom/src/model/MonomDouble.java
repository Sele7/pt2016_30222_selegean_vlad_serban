package model;

// TODO: Auto-generated Javadoc
/**
 * Clasa Monom Double ce extinda clasa Monom pentru coeficienti de tip double , ce apar la integrare.
 */
public class MonomDouble extends Monom{

	/** The coeficient. */
	private Double coeficient;

	/**
	 * Constructorul clasei..
	 *
	 * @param coeficient the coeficient
	 * @param putere the putere
	 */
	public MonomDouble(Double coeficient,int putere) {
		super(coeficient,putere);
		this.coeficient = coeficient;
		this.putere = putere;
		
	}

	/* (non-Javadoc)
	 * @see model.Monom#getCoeficient()
	 */
	public Double getCoeficient() {
		return coeficient;
	}

	/**
	 * Sets the coeficient.
	 *
	 * @param coeficient the new coeficient
	 */
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	
	
	
}
