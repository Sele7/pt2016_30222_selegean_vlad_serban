package model;

import java.util.List;
/** Interfata pentur functiile folosite de Calculator
 *  Se obsearva ca aproape toate metodele returneaza un polinom inafara de impartire deoarece
 *  trebuie sa returnam 2 polinoame adica un cat si un rest*/
public interface Metode {
	
	Polynom adunare(Polynom a,Polynom b);
	Polynom scadere(Polynom a,Polynom b);
	Polynom inmultire(Polynom a,Polynom b);
	List<Polynom> impartire(Polynom a,Polynom b);
	Polynom derivare(Polynom a);
	Polynom integrare(Polynom a);

}
