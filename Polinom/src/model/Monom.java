package model;

// TODO: Auto-generated Javadoc
/**
 * Clasa monom ce prezinta un coeficient de tip Number pentru a ne fi mai usor cand facem operatii.
 * si o putere
 */
public class Monom {

	/** Cum spuneam Coeficientul este Number astfel putem sa il facem Double sau Int dupa nevoie */
	protected Number coeficient;
	
	/** Putere respectiva pt un monom */
	protected int putere;
	
	/**
	 * Constructorul Monomului ...
	 *
	 * @param coeficient the coeficient
	 * @param putere the putere
	 */
	public Monom(Number coeficient,int putere){
		this.coeficient = coeficient;
		this.putere = putere;
	}


	/**
	 * Getterul pentru coeficient.
	 *
	 * @return the coeficient
	 */
	public Number getCoeficient() {
		return coeficient;
	}

	/**
	 * Setterul pentru coeficient.
	 *
	 * @param coeficient the new coeficient
	 */
	public void setCoeficient(Number coeficient) {
		this.coeficient = coeficient;
	}

	/**
	 * Getterul pentru putere.
	 *
	 * @return the putere
	 */
	public int getPutere() {
		return putere;
	}

	/**
	 * Setterul pentru putere.
	 *
	 * @param putere the new putere
	 */
	public void setPutere(int putere) {
		this.putere = putere;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	/** to.String-ul returneaza un sir de forma -coef-x^-putere-*/
	public String toString() {
		return getCoeficient() + "x^" + getPutere();
	}
	
	/**
	 * Am facut o metoda de a returna coeficientul din parametru , folosit la inmultire pentru a putea face suma
	 * coeficientiilor pentru x-uri de acc putere
	 * @param coeficient the coeficient
	 * @return the int
	 */
	public int coef(Number coeficient) {
		this.coeficient = coeficient;
		return (int) this.coeficient;
	}

}
