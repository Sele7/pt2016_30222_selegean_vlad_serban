package model;

// TODO: Auto-generated Javadoc
/**
 * Clasa MonomInt folosita in general inafara de integrare deoarece acolo v-om avea coef. double.
 */
public class MonomInt extends Monom{

	/** The coeficient. */
	private Integer coeficient;
	

	/**
	 * Constructorul clasei..
	 *
	 * @param coeficient the coeficient
	 * @param putere the putere
	 */
	protected MonomInt(Integer coeficient,int putere) {
		super(coeficient,putere);
		this.coeficient = coeficient;
		this.putere = putere;
	}

	/* (non-Javadoc)
	 * @see model.Monom#getCoeficient()
	 */
	public Integer getCoeficient() {
		return coeficient;
	}

	/**
	 * Sets the coeficient.
	 *
	 * @param coeficient the new coeficient
	 */
	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}
	
	
	
	
	
	
}
